﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;
using CarDealership.Models.Interfaces;

namespace CarDealership.DAL
{
    public class VehicleMockRepository : IVehicleRepository
    {
        private List<FeaturedVehicle> _testFeaturedList;
        private List<Vehicle> _testVehicleList;

        public VehicleMockRepository()
        {
            if (_testFeaturedList == null)
            {
                _testFeaturedList = new List<FeaturedVehicle>()
                {
                    new FeaturedVehicle(){Id = 1, Year = 2016, ImageUrl = "", Make = "Toyota", Model = "Camry", Price = 21000},
                    new FeaturedVehicle(){Id = 2, Year = 2015, ImageUrl = "", Make = "Toyota", Model = "Corolla", Price = 25000}
                };
            }
            if (_testVehicleList == null)
            {
                _testVehicleList = new List<Vehicle>()
                {
                    new Vehicle(){Id = 1, Year = 2016, ImageUrl = "", Make = "Toyota", Model = "Camry", Price = 21000, Mileage = 500,  BodyStyle = "Car", Color = "Black", Featured = false, Msrp = 23000, Transmission = true, Vin = "1C4NJPBA0CD722387"},
                    new Vehicle(){Id = 2, Year = 2015, ImageUrl = "", Make = "Toyota", Model = "Corolla", Price = 25000, Mileage = 4000, BodyStyle = "Car", Color = "Almond", Featured = true, Msrp = 28000, Transmission = false, Vin = "1FTWX31Y38EA92456"}
                };
            }

        }

        public List<FeaturedVehicle> GetFeaturedVehicles()
        {
            return _testFeaturedList;
        }

        public List<Vehicle> GetNewVehicles()
        {
            List<Vehicle> list = new List<Vehicle>();
            foreach (var v in _testVehicleList)
            {
                if (v.Mileage <= 1000)
                {
                    list.Add(v);
                }
            }
            return list;
        }

        public List<Vehicle> GetUsedVehicles()
        {
            List<Vehicle> list = new List<Vehicle>();
            foreach (var v in _testVehicleList)
            {
                if (v.Mileage >= 1000)
                {
                    list.Add(v);
                }
            }
            return list;
        }

        public Vehicle GetById(int id)
        {
            return _testVehicleList.Find(v => v.Id == id);
        }

        public List<Vehicle> GetSaleVehicles()
        {
            return _testVehicleList;
        }

        public int AddVehicle(Vehicle vehicle)
        {
            int number = 0;
            _testVehicleList.Add(vehicle);
            number = _testVehicleList.Last().Id;
            return number;
        }

        public void EditVehicle(Vehicle vehicle, int id)
        {
            int x = _testVehicleList.IndexOf(_testVehicleList.FirstOrDefault(t => t.Id == id));
            _testVehicleList[x].Model = vehicle.Model;
            _testVehicleList[x].BodyStyle = vehicle.BodyStyle;
            _testVehicleList[x].Color = vehicle.Color;
            _testVehicleList[x].Description= vehicle.Description;
            _testVehicleList[x].Featured = vehicle.Featured;
            _testVehicleList[x].Interior = vehicle.Interior;
            _testVehicleList[x].Make = vehicle.Make;
            _testVehicleList[x].Mileage = vehicle.Mileage;
            _testVehicleList[x].Msrp = vehicle.Msrp;
            _testVehicleList[x].Price = vehicle.Price;
            _testVehicleList[x].Transmission = vehicle.Transmission;
            _testVehicleList[x].Year = vehicle.Year;
        }

        public void DeleteVehicle(int id)
        {
            _testVehicleList.Remove(_testVehicleList.First(t => t.Id == id));
        }
    }
}
