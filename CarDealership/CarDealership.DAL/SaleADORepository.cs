﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;
using CarDealership.Models.Interfaces;

namespace CarDealership.DAL
{
    public class SaleADORepository : ISaleRepository
    {
        private string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public void CreateSale(Sale sale)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("PostSale", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SaleName", sale.Name);
                cmd.Parameters.AddWithValue("@SalesPerson", sale.SalesPerson);
                cmd.Parameters.AddWithValue("@PurchasePrice", sale.PurchasePrice);
                cmd.Parameters.AddWithValue("@City", sale.City);
                if (sale.Email == null)
                {
                    cmd.Parameters.AddWithValue("@Email", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Email", sale.Email);
                }
                if (sale.Phone == null)
                {
                    cmd.Parameters.AddWithValue("@Phone", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Phone", sale.Phone);
                }

                cmd.Parameters.AddWithValue("@PurchaseType", sale.PurchaseType);
                cmd.Parameters.AddWithValue("@State", sale.State);
                cmd.Parameters.AddWithValue("@Street1", sale.Street1);
                if (sale.Street2 == null)
                {
                    cmd.Parameters.AddWithValue("@Street2", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Street2", sale.Street2);
                }
                cmd.Parameters.AddWithValue("@Zipcode", sale.Zipcode);
                cmd.Parameters.AddWithValue("@VehicleId", sale.VehicleId);
                try
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
                catch (Exception e)
                {
                    string error = e.Message;
                }

            }
        }
    }
}
