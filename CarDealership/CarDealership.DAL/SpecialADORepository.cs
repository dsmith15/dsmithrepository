﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using CarDealership.Models;
using CarDealership.Models.Interfaces;

namespace CarDealership.DAL
{
    public class SpecialADORepository : ISpecialRepository
    {
        private string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public List<Special> GetAll()
        {
            List<Special> list = new List<Special>();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetAllSpecials", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Special currentRow = new Special();
                        currentRow.Id = int.Parse(dr["SpecialId"].ToString());
                        currentRow.Title = dr["SpecialTitle"].ToString();
                        currentRow.Description = dr["SpecialDescription"].ToString();
                        list.Add(currentRow);
                    }
                }
            }
            return list;
        }

        public void AddSpecial(Special special)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("AddSpecial", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SpecialDescription", special.Description);
                cmd.Parameters.AddWithValue("@SpecialTitle", special.Title);
                try
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
                catch (Exception e)
                {
                    string error = e.Message;
                }
            }
        }

        public void DeleteSpecial(int id)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("DeleteSpecial", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SpecialId", id);
                try
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
                catch (Exception e)
                {
                    string error = e.Message;
                }
            }
        }
    }
}
