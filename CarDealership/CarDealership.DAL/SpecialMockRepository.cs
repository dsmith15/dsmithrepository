﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;
using CarDealership.Models.Interfaces;

namespace CarDealership.DAL
{
    public class SpecialMockRepository : ISpecialRepository
    {
        private List<Special> _testList = new List<Special>();
        public SpecialMockRepository()
        {
            if (_testList == null)
            {
                _testList = new List<Special>()
                {
                    new Special(){Id = 1, Title= "Great Deal", Description = "Special on a Toyota Camry! Buy now and save $500!!!"},
                    new Special(){Id = 2, Title = "No Interest NoVember!", Description = "Come in and shop our deals! Select vehicles are available at 0% interest for the first 9 months during No Interest November"}
                };
            }
        }

        public List<Special> GetAll()
        {
            return _testList;
        }

        public void AddSpecial(Special special)
        {
            _testList.Add(special);
        }

        public void DeleteSpecial(int id)
        {
            _testList.Remove(_testList.First(t => t.Id == id));
        }
    }
}
