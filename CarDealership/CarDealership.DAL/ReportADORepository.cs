﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;
using CarDealership.Models.Interfaces;

namespace CarDealership.DAL
{
    public class ReportADORepository : IReportRepository
    {
        private string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public List<InventoryReport> GetNewInventory()
        {
            List<InventoryReport> list = new List<InventoryReport>();

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("NewInventoryReport", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        InventoryReport currentRow = new InventoryReport();
                        currentRow.Year = int.Parse(dr["ModelYear"].ToString());
                        currentRow.Make = dr["MakeDescription"].ToString();
                        currentRow.Model = dr["ModelDescription"].ToString();
                        currentRow.Count = int.Parse(dr["TotalVehicles"].ToString());
                        currentRow.StockValue = int.Parse(dr["TotalValue"].ToString());

                        list.Add(currentRow);
                    }
                }
            }
            return list;
        }

        public List<InventoryReport> GetUsedInventory()
        {
            List<InventoryReport> list = new List<InventoryReport>();

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("OldInventoryReport", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        InventoryReport currentRow = new InventoryReport();
                        currentRow.Year = int.Parse(dr["ModelYear"].ToString());
                        currentRow.Make = dr["MakeDescription"].ToString();
                        currentRow.Model = dr["ModelDescription"].ToString();
                        currentRow.Count = int.Parse(dr["TotalVehicles"].ToString());
                        currentRow.StockValue = int.Parse(dr["TotalValue"].ToString());

                        list.Add(currentRow);
                    }
                }
            }
            return list;
        }

        public List<SalesReport> GetSalesReports(string user, DateTime startDate, DateTime endDate)
        {
            List<SalesReport> list = new List<SalesReport>();


            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("SalesReport", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                if (user == null)
                {
                    cmd.Parameters.AddWithValue("@User", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@User", user);
                }
                cmd.Parameters.AddWithValue("@StartDate", startDate);
                cmd.Parameters.AddWithValue("@EndDate", endDate);

                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SalesReport currentRow = new SalesReport();
                        currentRow.User = dr["SalesPerson"].ToString();
                        currentRow.TotalSales = int.Parse(dr["TotalValue"].ToString());
                        currentRow.TotalVehicles = int.Parse(dr["TotalVehicles"].ToString());

                        list.Add(currentRow);
                    }
                }
            }
            return list;
        }
    }
}
