﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;
using CarDealership.Models.Interfaces;

namespace CarDealership.DAL
{
    public class ReportMockRepository : IReportRepository
    {
        List<InventoryReport> _inventoryReports = new List<InventoryReport>();
        List<SalesReport> _salesReports = new List<SalesReport>();

        public ReportMockRepository()
        {
            if (_inventoryReports == null)
            {
                _inventoryReports = new List<InventoryReport>();
                InventoryReport tag1 = new InventoryReport();
                InventoryReport tag2 = new InventoryReport();
                tag1.Model = "Test";
                tag1.Count = 4;
                tag1.Make = "Test";
                tag1.StockValue = 2323;
                tag1.Year = 2012;
                tag2.Model = "Test";
                tag2.Count = 4;
                tag2.Make = "Test";
                tag2.StockValue = 2323;
                tag2.Year = 2012;
                _inventoryReports.Add(tag1);
                _inventoryReports.Add(tag2);
            }
            if (_salesReports == null)
            {
                _salesReports = new List<SalesReport>();
                SalesReport tag1 = new SalesReport();
                SalesReport tag2 = new SalesReport();
                tag1.User = "Test";
                tag1.TotalSales = 45;
                tag1.TotalVehicles = 46;
                tag2.User = "Test";
                tag2.TotalSales = 45;
                tag2.TotalVehicles = 46;
                _salesReports.Add(tag1);
                _salesReports.Add(tag2);
            }
        }

        public List<InventoryReport> GetNewInventory()
        {
            return _inventoryReports;
        }

        public List<InventoryReport> GetUsedInventory()
        {
            return _inventoryReports;
        }

        public List<SalesReport> GetSalesReports(string user, DateTime startDate, DateTime endDate)
        {
            return _salesReports;
        }
    }
}
