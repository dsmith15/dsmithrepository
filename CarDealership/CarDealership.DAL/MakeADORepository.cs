﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;
using CarDealership.Models.Interfaces;

namespace CarDealership.DAL
{
    public class MakeADORepository : IMakeRepository
    {
        private string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public List<Make> GetAll()
        {
            List<Make> makeList = new List<Make>();

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetAllMakes", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Make currentRow = new Make();
                        currentRow.Description = dr["MakeDescription"].ToString();
                        currentRow.User = dr["MakeUser"].ToString();
                        currentRow.CreateDateTime = DateTime.Parse(dr["MakeCreatedDate"].ToString());

                        makeList.Add(currentRow);
                    }
                }
            }
            return makeList;
        }

        public void AddMake(Make make)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("AddMake", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MakeDescription", make.Description);
                cmd.Parameters.AddWithValue("@MakeUser", make.User);
                try
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
                catch (Exception e)
                {
                    string error = e.Message;
                }
            }
        }
    }
}
