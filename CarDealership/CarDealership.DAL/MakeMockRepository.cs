﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;
using CarDealership.Models.Interfaces;

namespace CarDealership.DAL
{
    public class MakeMockRepository : IMakeRepository
    {
        private List<Make> _list = new List<Make>();

        public MakeMockRepository()
        {
            if (_list == null)
            {
                _list = new List<Make>();
                Make tag1 = new Make();
                Make tag2 = new Make();
                tag1.User = "Test";
                tag1.Description = "Test";
                tag2.User = "Test";
                tag2.Description = "Test";
                _list.Add(tag1);
                _list.Add(tag2);
            }
        }


        public List<Make> GetAll()
        {
            return _list;
        }

        public void AddMake(Make make)
        {
            _list.Add(make);
        }
    }
}
