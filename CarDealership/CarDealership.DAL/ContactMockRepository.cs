﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;
using CarDealership.Models.Interfaces;

namespace CarDealership.DAL
{
    public class ContactMockRepository : IContactRepository
    {
        private List<Contact> _list = new List<Contact>();

        public ContactMockRepository()
        {
            if (_list == null)
            {
                _list = new List<Contact>();
                Contact tag1 = new Contact();
                Contact tag2 = new Contact();
                tag1.Name = "Test";
                tag1.Email = "Test";
                tag1.Notes = "Test";
                tag1.Phone = "Test";
                tag2.Name = "Test";
                tag2.Email = "Test";
                tag2.Notes = "Test";
                tag2.Phone = "Test";
                _list.Add(tag1);
                _list.Add(tag2);
            }
        }

        public void PostContact(Contact contact)
        {
            _list.Add(contact);
        }
    }
}
