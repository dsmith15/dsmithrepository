﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;
using CarDealership.Models.Interfaces;

namespace CarDealership.DAL
{
    public class UserADORepository : IUserRepository
    {
        private string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public List<User> GetAll()
        {
            List<User> list = new List<User>();

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetAllUsers", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        User currentRow = new User();
                        currentRow.Id = dr["Id"].ToString();
                        string name = dr["UserName"].ToString();
                        if (name.Contains(' '))
                        {
                            string[] names = name.Split(' ');
                            currentRow.FirstName = names[0];
                            if (names[1] != null)
                            {
                                currentRow.LastName = names[1];
                            }
                        }
                        else
                        {
                            currentRow.FirstName = dr["UserName"].ToString();
                        }
                        currentRow.Email = dr["Email"].ToString();
                        currentRow.Role = dr["Name"].ToString();
                        bool dontAddRow = false;
                        foreach (var u in list)
                        {
                            if (u.FirstName == currentRow.FirstName)
                            {
                                if (currentRow.Role == "Disabled" || u.Role == "Disabled")
                                {
                                    u.Role = "Disabled";
                                    dontAddRow = true;
                                }
                                else
                                {
                                    u.Role = u.Role + ", " + currentRow.Role;
                                    dontAddRow = true;
                                }
                            }
                            
                        }
                        if (dontAddRow)
                        {
                            continue;
                        }
                        else
                        {
                            list.Add(currentRow);
                        }
                    }
                }
            }
            return list;
        }

        public void AddUser(User user)
        {
            throw new NotImplementedException();
        }

        public void EditUser(User user, int id)
        {
            throw new NotImplementedException();
        }

        public User GetById(string id)
        {
            User user = new User();

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetUserById", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        user.Id = dr["Id"].ToString();
                        string name = dr["UserName"].ToString();
                        string[] names = name.Split(' ');
                        user.FirstName = names[0];
                        if (names[1] != null)
                        {
                            user.LastName = names[1];
                        }
                        user.Email = dr["Email"].ToString();
                        user.Role = dr["Name"].ToString();
                    }
                }
            }
            return user;
        }
    }
}
