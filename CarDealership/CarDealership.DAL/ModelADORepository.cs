﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;
using CarDealership.Models.Interfaces;

namespace CarDealership.DAL
{
    public class ModelADORepository : IModelRepository
    {
        private string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public List<Model> GetAllByMake(string make)
        {
            List<Model> list = new List<Model>();

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetModelsByMake", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                if (make == null)
                {
                    cmd.Parameters.AddWithValue("@MakeDescription", DBNull.Value);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@MakeDescription", make);
                }
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    try
                    {
                        while (dr.Read())
                        {
                            Model currentRow = new Model();
                            currentRow.Description = dr["ModelDescription"].ToString();
                            list.Add(currentRow);
                        }
                    }
                    catch (Exception e)
                    {
                        string message = e.Message;
                    }
                }
            }
            return list;
        }

        public List<Model> GetAll()
        {
            List<Model> list = new List<Model>();

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetModels", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    try
                    {
                        while (dr.Read())
                        {
                            Model currentRow = new Model();
                            currentRow.Description = dr["ModelDescription"].ToString();
                            currentRow.User = dr["ModelUser"].ToString();
                            currentRow.CreateDateTime = DateTime.Parse(dr["ModelCreatedDate"].ToString());
                            currentRow.MakeDescription = dr["MakeDescription"].ToString();

                            list.Add(currentRow);
                        }
                    }
                    catch (Exception e)
                    {
                        string message = e.Message;
                    }
                }
            }
            return list;

        }

        public void AddModel(Model model)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("AddModel", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ModelDescription", model.Description);
                cmd.Parameters.AddWithValue("@ModelUser", model.User);
                cmd.Parameters.AddWithValue("@MakeDescription", model.MakeDescription);
                try
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
                catch (Exception e)
                {
                    string error = e.Message;
                }
            }
        }
    }
}
