﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;
using CarDealership.Models.Interfaces;

namespace CarDealership.DAL
{
    public class VehicleADORepository : IVehicleRepository
    {
        private string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public List<FeaturedVehicle> GetFeaturedVehicles()
        {
            List<FeaturedVehicle> list = new List<FeaturedVehicle>();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetFeaturedVehicles", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {

                        FeaturedVehicle currentRow = new FeaturedVehicle();
                        currentRow.Id = int.Parse(dr["VehicleId"].ToString());
                        currentRow.ImageUrl = dr["Picture"].ToString();
                        currentRow.Make = dr["MakeDescription"].ToString();
                        currentRow.Model = dr["ModelDescription"].ToString();
                        currentRow.Price = decimal.Parse(dr["Price"].ToString());
                        currentRow.Year = int.Parse(dr["ModelYear"].ToString());
                        list.Add(currentRow);
                    }
                }
            }
            return list;

        }

        public List<Vehicle> GetNewVehicles()
        {
            List<Vehicle> list = new List<Vehicle>();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetNewVehicles", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        list.Add(VehicleMapper.MapVehicle(dr));
                    }
                }
            }
            return list;
        }

        public List<Vehicle> GetUsedVehicles()
        {
            List<Vehicle> list = new List<Vehicle>();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetUsedVehicles", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        list.Add(VehicleMapper.MapVehicle(dr));
                    }
                }
            }
            return list;
        }

        public List<Vehicle> GetSaleVehicles()
        {
            List<Vehicle> list = new List<Vehicle>();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetPurchaseVehicles", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    try
                    {
                        while (dr.Read())
                        {
                            list.Add(VehicleMapper.MapVehicle(dr));
                        }
                    }
                    catch (Exception e)
                    {
                        string message = e.Message;
                    }
                }
            }
            return list;
        }

        public int AddVehicle(Vehicle vehicle)
        {
            int id = 0;
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("AddVehicle", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Model", vehicle.Model);
                cmd.Parameters.AddWithValue("@BodyStyle", vehicle.BodyStyle);
                cmd.Parameters.AddWithValue("@Color", vehicle.Color);
                cmd.Parameters.AddWithValue("@Description", vehicle.Description);
                cmd.Parameters.AddWithValue("@Featured", vehicle.Featured);
                if (vehicle.ImageUrl == null)
                {
                    cmd.Parameters.AddWithValue("@ImageUrl", " ");

                }
                else
                {
                    cmd.Parameters.AddWithValue("@ImageUrl", vehicle.ImageUrl);
                }
                cmd.Parameters.AddWithValue("@Interior", vehicle.Interior);
                cmd.Parameters.AddWithValue("@Mileage", vehicle.Mileage);
                cmd.Parameters.AddWithValue("@Msrp", vehicle.Msrp);
                cmd.Parameters.AddWithValue("@Price", vehicle.Price);
                cmd.Parameters.AddWithValue("@Transmission", vehicle.Transmission);
                cmd.Parameters.AddWithValue("@Vin", vehicle.Vin);
                cmd.Parameters.AddWithValue("@Year", vehicle.Year);
                try
                {
                    cn.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            id = int.Parse(dr["VehicleId"].ToString());
                        }
                    }
                    cn.Close();
                }
                catch (Exception e)
                {
                    string error = e.Message;
                }
            }
            return id;
        }

        public void EditVehicle(Vehicle vehicle, int id)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("EditVehicle", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@VehicleId", vehicle.Id);
                cmd.Parameters.AddWithValue("@Model", vehicle.Model);
                cmd.Parameters.AddWithValue("@BodyStyle", vehicle.BodyStyle);
                cmd.Parameters.AddWithValue("@Color", vehicle.Color);
                cmd.Parameters.AddWithValue("@Description", vehicle.Description);
                cmd.Parameters.AddWithValue("@Featured", vehicle.Featured);
                if (vehicle.ImageUrl == null)
                {
                    cmd.Parameters.AddWithValue("@ImageUrl", " ");

                }
                else
                {
                    cmd.Parameters.AddWithValue("@ImageUrl", vehicle.ImageUrl);
                }
                cmd.Parameters.AddWithValue("@Interior", vehicle.Interior);
                cmd.Parameters.AddWithValue("@Mileage", vehicle.Mileage);
                cmd.Parameters.AddWithValue("@Msrp", vehicle.Msrp);
                cmd.Parameters.AddWithValue("@Price", vehicle.Price);
                cmd.Parameters.AddWithValue("@Transmission", vehicle.Transmission);
                cmd.Parameters.AddWithValue("@Vin", vehicle.Vin);
                cmd.Parameters.AddWithValue("@Year", vehicle.Year);
                try
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
                catch (Exception e)
                {
                    string error = e.Message;
                }
            }
        }

        public void DeleteVehicle(int id)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("DeleteVehicle", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@VehicleId", id);
                try
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
                catch (Exception e)
                {
                    string error = e.Message;
                }
            }
        }

        public Vehicle GetById(int id)
        {
            var vehicle = new Vehicle();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetVehicleById", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", id);
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        vehicle = VehicleMapper.MapVehicle(dr);
                    }
                }
            }
            return vehicle;
        }
    }
}
