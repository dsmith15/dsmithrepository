﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;
using CarDealership.Models.Interfaces;

namespace CarDealership.DAL
{
    public class UserMockRepository : IUserRepository
    {
        private List<User> _list = new List<User>();

        public UserMockRepository()
        {
            if (_list == null)
            {
                _list = new List<User>();
                User tag1 = new User();
                User tag2 = new User();
                tag1.Id = "stuff";
                tag1.Email = "Test";
                tag1.FirstName = "Test";
                tag1.LastName = "Test";
                tag1.Password = "Test";
                tag1.Role = "Test";
                tag2.Id = "stuff";
                tag2.Email = "Test";
                tag2.FirstName = "Test";
                tag2.LastName = "Test";
                tag2.Password = "Test";
                tag2.Role = "Test";
                _list.Add(tag1);
                _list.Add(tag2);
            }
        }

        public List<User> GetAll()
        {
            return _list;
        }

        public void AddUser(User user)
        {
            _list.Add(user);
        }

        public void EditUser(User user, int id)
        {
            int x = _list.IndexOf(_list.FirstOrDefault(t => t.Id == id.ToString()));
            _list[x].Email = user.Email;
            _list[x].FirstName = user.FirstName;
            _list[x].LastName = user.LastName;
            _list[x].Password = user.Password;
            _list[x].Role = user.Role;
        }

        public User GetById(string id)
        {
            User user = _list.FirstOrDefault(t => t.Id == id);
            return user;
        }
    }
}
