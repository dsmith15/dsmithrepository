﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;
using CarDealership.Models.Interfaces;

namespace CarDealership.DAL
{
    public class SaleMockRepository : ISaleRepository
    {
        private List<Sale> _testList = new List<Sale>();
        public SaleMockRepository()
        {
            if (_testList == null)
            {
                _testList = new List<Sale>()
                {
                    new Sale(){SaleId = 1, Name= "Joe Smith", Email = "jsmith@gmail.com", Phone = "330-569-5969", Street1 = "123 Main St", Street2 = "", City = "Akron", State = "Ohio", Zipcode = "44311", PurchasePrice = 12000, PurchaseType = "Cash"},
                    new Sale(){SaleId = 2, Name= "Jane Doe", Email = "jdoe@gmail.com", Phone = "330-589-1483", Street1 = "456 High St", Street2 = "Apartment 210", City = "Akron", State = "Ohio", Zipcode = "44310", PurchasePrice = 16000, PurchaseType = "Bank Finance"}
                };
            }
        }

        public void CreateSale(Sale sale)
        {
            _testList.Add(sale);
        }
    }
}
