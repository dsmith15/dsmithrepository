﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.Models;
using CarDealership.Models.Interfaces;

namespace CarDealership.DAL
{
    public class ModelMockRepository : IModelRepository
    {
        private List<Model> _list = new List<Model>();

        public ModelMockRepository()
        {
            if (_list == null)
            {
                _list = new List<Model>();
                Model tag1 = new Model();
                Model tag2 = new Model();
                tag1.User = "Test";
                tag1.Description = "Test";
                tag1.MakeDescription = "Test";
                tag2.User = "Test";
                tag2.Description = "Test";
                tag2.MakeDescription = "Test";
                _list.Add(tag1);
                _list.Add(tag2);
            }
        }

        public List<Model> GetAllByMake(string make)
        {
            return _list.Where(m => m.MakeDescription == make).ToList();
        }

        public List<Model> GetAll()
        {
            return _list;
        }

        public void AddModel(Model model)
        {
            _list.Add(model);
        }
    }
}
