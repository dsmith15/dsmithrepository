﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.DAL;

namespace CarDealership.BLL
{
    public class ManagerFactory
    {
        public static Manager CreateManager()
        {
            string mode = ConfigurationManager.AppSettings["Mode"].ToString();

            switch (mode)
            {
                case "QA":
                    return new Manager(new SpecialMockRepository(), new VehicleMockRepository(), new SaleMockRepository(), new MakeMockRepository(), new ModelMockRepository(), new UserMockRepository(), new ReportMockRepository(), new ContactMockRepository());
                case "PROD":
                    return new Manager(new SpecialADORepository(), new VehicleADORepository(), new SaleADORepository(), new MakeADORepository(), new ModelADORepository(), new UserADORepository(), new ReportADORepository(), new ContactADORepository());
                default:
                    throw new Exception("Mode value in web config is not valid.");
            }
        }

    }
}
