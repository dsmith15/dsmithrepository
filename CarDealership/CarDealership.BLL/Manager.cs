﻿using CarDealership.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using CarDealership.Models.Interfaces;

namespace CarDealership.BLL
{
    public class Manager
    {
        private ISpecialRepository _specialRepository;
        private IVehicleRepository _vehicleRepository;
        private ISaleRepository _saleRepository;
        private IMakeRepository _makeRepository;
        private IModelRepository _modelRepository;
        private IUserRepository _userRepository;
        private IReportRepository _reportRepository;
        private IContactRepository _contactRepository;

        public Manager(ISpecialRepository specialRepository, IVehicleRepository vehicleRepository, ISaleRepository saleRepository, IMakeRepository makeRepository, IModelRepository modelRepository, IUserRepository userRepository, IReportRepository reportRepository, IContactRepository contactRepository)
        {
            _specialRepository = specialRepository;
            _vehicleRepository = vehicleRepository;
            _saleRepository = saleRepository;
            _makeRepository = makeRepository;
            _modelRepository = modelRepository;
            _userRepository = userRepository;
            _reportRepository = reportRepository;
            _contactRepository = contactRepository;
        }

        public List<Special> GetAllSpecials()
        {
            List<Special> list = _specialRepository.GetAll();
            return list;
        }

        public List<FeaturedVehicle> GetFeaturedVehicles()
        {
            List<FeaturedVehicle> list = _vehicleRepository.GetFeaturedVehicles();
            return list;
        }

        public List<Vehicle> GetNewVehicles(int? minYear, int? maxYear, decimal? minPrice, decimal? maxPrice, string makeModelYear)
        {
            var totalList = _vehicleRepository.GetNewVehicles();
            IQueryable<Vehicle> query = totalList.AsQueryable();
            if (minYear.HasValue)
            {
                query = query.Where(r => r.Year > minYear.Value);   
            }
            if (maxYear.HasValue)
            {
                query = query.Where(r => r.Year < maxYear.Value);
            }
            if (minPrice.HasValue)
            {
                query = query.Where(r => r.Price > minPrice.Value);
            }
            if (maxPrice.HasValue)
            {
                query = query.Where(r => r.Price < maxPrice.Value);
            }
            if (!string.IsNullOrEmpty(makeModelYear))
            {
                query = query.Where(r => r.Make.Contains(makeModelYear) || r.Model.Contains(makeModelYear) || r.Year.ToString().Contains(makeModelYear));
            }

            List<Vehicle> results = query.ToList();
            totalList = results.OrderByDescending(v => v.Msrp).ToList();
            if (totalList.Count > 20)
            {
                totalList.RemoveRange(20, totalList.Count - 20);
            }
            return totalList;
        }

        public List<Vehicle> GetUsedVehicles(int? minYear, int? maxYear, decimal? minPrice, decimal? maxPrice, string makeModelYear)
        {
            var totalList = _vehicleRepository.GetUsedVehicles();
            IQueryable<Vehicle> query = totalList.AsQueryable();
            if (minYear.HasValue)
            {
                query = query.Where(r => r.Year > minYear.Value);
            }
            if (maxYear.HasValue)
            {
                query = query.Where(r => r.Year < maxYear.Value);
            }
            if (minPrice.HasValue)
            {
                query = query.Where(r => r.Price > minPrice.Value);
            }
            if (maxPrice.HasValue)
            {
                query = query.Where(r => r.Price < maxPrice.Value);
            }
            if (!string.IsNullOrEmpty(makeModelYear))
            {
                query = query.Where(r => r.Make.Contains(makeModelYear) || r.Model.Contains(makeModelYear) || r.Year.ToString().Contains(makeModelYear));
            }
            List<Vehicle> results = query.ToList();
            totalList = results.OrderByDescending(v => v.Msrp).ToList();
            if (totalList.Count > 20)
            {
                totalList.RemoveRange(20, totalList.Count - 20);
            }
            return totalList;
        }

        public Vehicle GetVehicleById(int id)
        {
            var needsChecked = GetSaleVehicles(null, null, null, null, null);
            var vehicle = _vehicleRepository.GetById(id);
            vehicle.IsSold = true;
            foreach (var v in needsChecked)
            {
                if (v.Id == vehicle.Id)
                {
                    vehicle.IsSold = false;
                }
            } 
            return vehicle;
        }

        public void PostSale(Sale sale)
        {
            _saleRepository.CreateSale(sale);
        }

        public List<Vehicle> GetSaleVehicles(int? minYear, int? maxYear, decimal? minPrice, decimal? maxPrice, string makeModelYear)
        {
            var totalList = _vehicleRepository.GetSaleVehicles();
            IQueryable<Vehicle> query = totalList.AsQueryable();
            if (minYear.HasValue)
            {
                query = query.Where(r => r.Year > minYear.Value);
            }
            if (maxYear.HasValue)
            {
                query = query.Where(r => r.Year < maxYear.Value);
            }
            if (minPrice.HasValue)
            {
                query = query.Where(r => r.Price > minPrice.Value);
            }
            if (maxPrice.HasValue)
            {
                query = query.Where(r => r.Price < maxPrice.Value);
            }
            if (!string.IsNullOrEmpty(makeModelYear))
            {
                query = query.Where(r => r.Make.Contains(makeModelYear) || r.Model.Contains(makeModelYear) || r.Year.ToString().Contains(makeModelYear));
            }

            List<Vehicle> results = query.ToList();
            totalList = results.OrderByDescending(v => v.Msrp).ToList();
            if (totalList.Count > 20)
            {
                totalList.RemoveRange(20, totalList.Count - 20);
            }
            return totalList;
        }

        public int CreateVehicle(Vehicle vehicle)
        {
            return _vehicleRepository.AddVehicle(vehicle);
        }

        public void EditVehicle(Vehicle vehicle)
        {
            _vehicleRepository.EditVehicle(vehicle, vehicle.Id);
        }

        public void DeleteVehicle(int id)
        {
            _vehicleRepository.DeleteVehicle(id);
        }

        public List<Make> GetMakes()
        {
            List<Make> list = _makeRepository.GetAll();
            return list;
        }

        public List<Model> GetAllModels()
        {
            List<Model> list = _modelRepository.GetAll();
            return list;
        }


        public List<Model> GetModels(string make)
        {
            List<Model> list = _modelRepository.GetAllByMake(make);
            return list;
        }

        public void AddMake(Make make)
        {
            _makeRepository.AddMake(make);
        }

        public void AddModel(Model model)
        {
            _modelRepository.AddModel(model);
        }

        public void AddSpecial(Special special)
        {
            _specialRepository.AddSpecial(special);
        }

        public Special GetSpecialById(int id)
        {
            return _specialRepository.GetAll().FirstOrDefault(s => s.Id == id);
        }

        public void DeleteSpecial(int id)
        {
            _specialRepository.DeleteSpecial(id);
        }

        public List<User> GetAllUsers()
        {
            return _userRepository.GetAll();
        }

        public User GetUserById(string id)
        {
            return _userRepository.GetById(id);
        }

        public List<InventoryReport> GetNewInventoryList()
        {
            return _reportRepository.GetNewInventory();
        }

        public List<InventoryReport> GetUsedInventoryList()
        {
            return _reportRepository.GetUsedInventory();
        }

        public List<SalesReport> GetSalesReports(string user, DateTime startDate, DateTime endDate)
        {
            return _reportRepository.GetSalesReports(user, startDate, endDate);
        }

        public void PostContact(Contact contact)
        {
            _contactRepository.PostContact(contact);
        }
    }
}
