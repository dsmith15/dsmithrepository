﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDealership.Models
{
    public class Vehicle
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public string ImageUrl { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public decimal Price { get; set; }
        public string BodyStyle { get; set; }
        public string Color { get; set; }
        public bool Transmission { get; set; }
        public bool Featured { get; set; }
        public decimal Msrp { get; set; }
        public string Vin { get; set; }
        public int Mileage { get; set; }
        public string Interior { get; set; }
        public string Description { get; set; }
        public bool IsSold { get; set; }
    }
}
