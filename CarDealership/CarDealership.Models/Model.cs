﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDealership.Models
{
    public class Model
    {
        public string Description { get; set; }
        public string User { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string MakeDescription { get; set; }
    }
}
