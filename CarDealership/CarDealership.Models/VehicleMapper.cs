﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDealership.Models
{
    public class VehicleMapper
    {
        public static Vehicle MapVehicle(SqlDataReader dr)
        {
            Vehicle currentRow = new Vehicle();
            currentRow.Id = int.Parse(dr["VehicleId"].ToString());
            currentRow.ImageUrl = dr["Picture"].ToString();
            currentRow.Make = dr["MakeDescription"].ToString();
            currentRow.Model = dr["ModelDescription"].ToString();
            currentRow.Price = decimal.Parse(dr["Price"].ToString());
            currentRow.Year = int.Parse(dr["ModelYear"].ToString());
            currentRow.Mileage = int.Parse(dr["Mileage"].ToString());
            currentRow.Featured = (bool)dr["Featured"];
            currentRow.Color = dr["ColorDescription"].ToString();
            currentRow.Transmission = (bool)dr["Transmission"];
            currentRow.BodyStyle = dr["BodyStyleDescription"].ToString();
            currentRow.Msrp = decimal.Parse(dr["MSRP"].ToString());
            currentRow.Vin = dr["VIN"].ToString();
            currentRow.Interior = dr["InteriorDescription"].ToString();
            currentRow.Description = dr["VehicleDescription"].ToString();


            return currentRow;
        }

    }
}
