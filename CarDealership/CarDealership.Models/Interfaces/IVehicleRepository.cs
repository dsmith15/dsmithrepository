﻿using System.Collections.Generic;
using CarDealership.Models;

namespace CarDealership.Models.Interfaces
{
    public interface IVehicleRepository
    {
        List<FeaturedVehicle> GetFeaturedVehicles();
        List<Vehicle> GetNewVehicles();
        List<Vehicle> GetUsedVehicles();
        Vehicle GetById(int id);
        List<Vehicle> GetSaleVehicles();
        int AddVehicle(Vehicle vehicle);
        void EditVehicle(Vehicle vehicle, int id);
        void DeleteVehicle(int id);

    }
}