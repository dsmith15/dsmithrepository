﻿using System.Collections.Generic;

namespace CarDealership.Models.Interfaces
{
    public interface ISpecialRepository
    {
        List<Special> GetAll();
        void AddSpecial(Special special);
        void DeleteSpecial(int id);
    }
}