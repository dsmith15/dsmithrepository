﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDealership.Models.Interfaces
{
    public interface IUserRepository
    {
        List<User> GetAll();
        void AddUser(User user);
        void EditUser(User user, int id);
        User GetById(string id);
    }
}
