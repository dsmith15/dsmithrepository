﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDealership.Models.Interfaces
{
    public interface IReportRepository
    {
        List<InventoryReport> GetNewInventory();
        List<InventoryReport> GetUsedInventory();
        List<SalesReport> GetSalesReports(string user, DateTime startDate, DateTime endDate);
    }
}
