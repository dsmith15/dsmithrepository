﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDealership.Models.Interfaces
{
    public interface IModelRepository
    {
        List<Model> GetAllByMake(string make);
        List<Model> GetAll();
        void AddModel(Model model);

    }
}
