﻿USE GuildCars
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES	
	Where ROUTINE_NAME = 'DBReset')
		DROP PROCEDURE DBReset
GO

CREATE PROCEDURE DBReset AS
BEGIN
	DELETE FROM Special;
	DELETE FROM Sale;
	DELETE FROM Vehicle;
	DELETE FROM Contact;
	DELETE FROM Model;
	DELETE FROM Make;
	DELETE FROM Interior;
	DELETE FROM BodyStyle;
	DELETE FROM Color;

set identity_insert Make on;

	INSERT INTO Make (MakeId, MakeDescription, MakeUser, MakeCreatedDate)
	Values (1, 'Toyota','admin@gcars.com','1-1-2017'),
	(2, 'Honda','admin@gcars.com','1-1-2017'),
	(3, 'Ford','admin@gcars.com','1-1-2017'),
	(4, 'Chevrolet','admin@gcars.com','1-1-2017'),
	(5, 'VolksWagen','admin@gcars.com','1-1-2017'),
	(6, 'Kia','admin@gcars.com','1-1-2017'),
	(7, 'Nissan','admin@gcars.com','1-1-2017')


	set identity_insert Make off;
	
set identity_insert Interior on;

	INSERT INTO Interior (InteriorId, InteriorDescription)
	Values (1, 'Black'),
	(2, 'Blue'),
	(3, 'Charcoal'),
	(4, 'Cinnamon'),
	(5, 'Wine'),
	(6, 'Palomino')

	set identity_insert Interior off;

set identity_insert BodyStyle on;

	INSERT INTO BodyStyle (BodyStyleId, BodyStyleDescription)
	Values (1, 'Car'),
	(2, 'SUV'),
	(3, 'Truck'),
	(4, 'Van')

	set identity_insert BodyStyle off;

set identity_insert Color on;

	INSERT INTO Color (ColorId, ColorDescription)
	Values (1, 'Black'),
	(2, 'Burgundy'),
	(3, 'Lime Green'),
	(4, 'Cherry Red'),
	(5, 'Blue'),
	(6, 'Silver'),
	(7, 'Almond')

	set identity_insert Color off;

set identity_insert Contact on;

	INSERT INTO Contact (ContactId, ContactName, Phone, Email, Notes)
	Values (1, 'Andrew Jones', '330-456-2135', 'ajones@email.com', 'I am very interested in this car'),
	(2,'David Smith', '440-563-5489', 'dsmith@company.com', 'Looking at this one in particular'),
	(3,'Danielle Johnson', '216-468-6458', 'djohnson@money.com', 'ooh, cool')

	set identity_insert Contact off;

set identity_insert Model on;

	INSERT INTO Model (ModelId, ModelDescription, ModelUser, ModelCreatedDate, MakeId)
	Values (1, 'Camry','admin@gcars.com','1-1-2017', 1),
	(2, 'Accord','admin@gcars.com','1-1-2017', 2),
	(3, 'Corolla','admin@gcars.com','1-1-2017', 1),
	(4, 'Fiesta','admin@gcars.com','1-1-2017', 3),
	(5, 'Beatle','admin@gcars.com','1-1-2017', 5),
	(6, 'Ranger','admin@gcars.com','1-1-2017', 3),
	(7, 'Sportage','admin@gcars.com','1-1-2017', 6),
	(8, 'Sentra','admin@gcars.com','1-1-2017', 7),
	(9, 'F-150','admin@gcars.com','1-1-2017', 3),
	(10, 'Venture','admin@gcars.com','1-1-2017', 4),
	(11, 'Sienna','admin@gcars.com','1-1-2017', 1)

	set identity_insert Model off;

set identity_insert Vehicle on;

	INSERT INTO Vehicle (VehicleId, ModelId, BodyStyleId, ColorId, InteriorId, Transmission, ModelYear, Picture, Mileage, VehicleDescription, Featured, Price, MSRP, VIN)
	Values (1, 1, 1, 7, 5, 0, 2001, '/uploads/Image_Vehicle1.jpg', 154909, 'Excellent used Toyota Camry. Still plenty of miles left in this one.', 1, 5000, 6500, '1HTSDAAL5YH223307'),
	(2, 2, 1, 1, 2, 0, 2015, '', 65008, 'Excellent used Honda Accord. Still plenty of miles left in this one.', 0, 10000, 12500, '1JCUB7830FT138551'),	
	(3, 6, 3, 4, 1, 1, 2003, '', 105000, 'Excellent used Ford Ranger. Still plenty of miles left in this one.', 1, 11500, 12000, 'JP3CU14X7KU156918'),	
	(4, 4, 1, 3, 6, 0, 2014, '', 75156, 'Excellent used Ford Fiesta. Still plenty of miles left in this one.', 1, 9500, 12000, '1GCDT19Z2P0167163'),	
	(5, 7, 2, 5, 4, 0, 2009, '', 45023, 'Excellent used Kia Sportage. Still plenty of miles left in this one.', 0, 10000, 11000, '1GDE6H1P2TJ555895'),	
	(6, 9, 3, 2, 1, 0, 2018, '', 235, 'Excellent new Ford F-150. Still plenty of miles left in this one.', 1, 45000, 50000, '1HTMSADR15J018554'),	
	(7, 8, 1, 5, 3, 1, 2018, '', 563, 'Excellent new Nissan Sentra. Still plenty of miles left in this one.', 1, 35000, 36000, 'WA1YD64B82N190510'),	
	(8, 10, 4, 6, 2, 0, 2005, '', 159809, 'Excellent used Chevrolet Venture. Still plenty of miles left in this one.', 0, 5500, 6500, '1C4RDHAG2CC676684'),	
	(9, 11, 4, 4, 5, 0, 2018, '', 254, 'Excellent new Toyota Sienna. Still plenty of miles left in this one.', 0, 32000, 35000, '1N6AA0CA0DN355519'),	
	(10, 5, 1, 3, 3, 1, 2016, '', 24809, 'Excellent used Volkswagen Beatle. Still plenty of miles left in this one.', 1, 11500, 13500, '1G2PE11R4HP230019'),	
	(11, 1, 1, 4, 4, 1, 2018, '', 100, 'Excellent new Toyota Camry. Still plenty of miles left in this one.', 1, 27500, 30000, '1HTAA17B3BHA29677')

	set identity_insert Vehicle off;

set identity_insert Sale on;

	INSERT INTO Sale (SaleId, VehicleId, PurchaseType, SaleName, Phone, Email, Street1, Street2, City, [State], Zipcode, PurchasePrice, PurchaseDate, SalesPerson)
	Values 
	(1, 7, 'Bank Finance', 'Jane Doe', '330-569-4586', 'DavidSSmith@rhyta.com', '123 Main St', '', 'Akron', 'Ohio', '44310', 5000, Convert(datetime,'Oct 23 5:50:00 2017'),'temp'),
	(2, 1, 'Cash','Jessica Trask', '303-965-5285', 'JessicaDTrask@dayrep.com', '456 High St', '', 'Medina', 'Ohio', '44256', 10000, Convert(datetime,'Oct 25 5:50:00 2017'),'temp'),	
	(3, 4, 'Dealer Finance','Francisco Housman', '315-354-0588', 'FranciscoLHousman@dayrep.com', '123 Main St', '', 'Stow', 'Ohio', '44224', 11500, Convert(datetime,'Oct 7 5:50:00 2017'),'temp'),	
	(4, 3, 'Dealer Finance','Monica Duck', '310-881-2631', 'MonicaNDuck@rhyta.com', '4324 State St', '', 'Barberton', 'Ohio', '44203', 9500, Convert(datetime,'Oct 26 5:50:00 2017'),'temp'),	
	(5, 5, 'Bank Finance','Kim Marino', '253-308-8796', 'KimFMarino@armyspy.com', '123 Main St', '', 'Mogadore', 'Ohio', '44260', 10000, Convert(datetime,'Oct 18 5:50:00 2017'),'temp'),	
	(6, 2, 'Dealer Finance','Wayne J. Winton', '859-386-8078', 'WayneJWinton@jourrapide.com', '456 High St', '', 'Akron', 'Ohio', '44310', 45000, Convert(datetime,'Oct 9 5:50:00 2017'),'temp'),	
	(7, 11, 'Cash','Thomas T. Weatherly', '231-274-5000', 'ThomasTWeatherly@dayrep.com', '4324 State St', '', 'Akron', 'Ohio', '44310', 35000, Convert(datetime,'Oct 17 5:50:00 2017'),'temp'),	
	(8, 6, 'Dealer Finance','Gilda J. Heck', '520-413-9891', 'GildaJHeck@jourrapide.com', '123 Main St', '', 'Copley', 'Ohio', '44320', 5500, Convert(datetime,'Oct 16 5:50:00 2017'),'temp'),	
	(9, 9, 'Bank Finance','Martha T. Robillard', '918-718-8496', 'MarthaTRobillard@jourrapide.com', '4324 State St', '', 'Akron', 'Ohio', '44310', 32000, Convert(datetime,'Oct 3 5:50:00 2017'),'temp'),	
	(10, 3, 'Dealer Finance','Kim M. Gill', '732-464-8015', 'KimMGill@jourrapide.com', '456 High St', '', 'Barberton', 'Ohio', '44203', 11500, Convert(datetime,'Oct 2 5:50:00 2017'),'temp'),	
	(11, 10, 'Cash','Gerald V. Rizzo', '770-383-7875', 'GeraldVRizzo@armyspy.com', '123 Main St', '', 'Akron', 'Ohio', '44310', 27500, Convert(datetime,'Oct 6 5:50:00 2017'),'temp')

	set identity_insert Sale off;

set identity_insert Special on;

	INSERT INTO Special (SpecialId, SpecialTitle, SpecialDescription)
	Values 
	(1, 'ToyotaThon', 'Take $500 off all Toyota Camries!!'),
	(2, 'NoInterest November!', 'No interest on all Rangers!!'),
	(3, 'OctoberFiesta!', 'We have extended our special deal on all Fiestas in stock!!'),
	(4, 'Soccer Mom Appreciation!', 'Take $500 off all Chevrolet Ventures!!'),
	(5, 'Born To Roam', 'Take $1000 off all Kia Sportages!!')

	set identity_insert Special off;

	END