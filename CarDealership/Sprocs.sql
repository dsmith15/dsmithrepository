﻿USE GuildCars
GO
IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetAllSpecials')
BEGIN
   DROP PROCEDURE GetAllSpecials
END
GO
CREATE PROCEDURE GetAllSpecials
AS
    SELECT *
    FROM Special
GO

IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetAllMakes')
BEGIN
   DROP PROCEDURE GetAllMakes
END
GO
CREATE PROCEDURE GetAllMakes
AS
    SELECT m.MakeDescription, m.MakeUser, m.MakeCreatedDate
    FROM Make m
GO

IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetModels')
BEGIN
   DROP PROCEDURE GetModels
END
GO
CREATE PROCEDURE GetModels
AS
    SELECT m.ModelDescription, m.ModelCreatedDate, m.ModelUser, k.MakeDescription
    FROM Model m
	INNER JOIN Make k ON k.MakeId = m.MakeId
GO

IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetAllUsers')
BEGIN
   DROP PROCEDURE GetAllUsers
END
GO
CREATE PROCEDURE GetAllUsers
AS
    SELECT u.Id, u.Email, u.UserName, r.[Name]
    FROM AspNetUsers u
	INNER JOIN AspNetUserRoles a ON a.UserId = u.Id
	INNER JOIN AspNetRoles r ON r.Id = a.RoleId
GO

IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetModelsByMake')
BEGIN
   DROP PROCEDURE GetModelsByMake
END
GO
CREATE PROCEDURE GetModelsByMake(
	@MakeDescription nvarchar(30))
AS
    SELECT m.ModelDescription
    FROM Model m
	INNER JOIN Make k ON k.MakeId = m.MakeId
	WHERE k.MakeDescription = @MakeDescription
GO


IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetFeaturedVehicles')
BEGIN
   DROP PROCEDURE GetFeaturedVehicles
END
GO
CREATE PROCEDURE GetFeaturedVehicles
AS
    SELECT v.VehicleId, v.ModelYear, v.Picture, k.MakeDescription, d.ModelDescription, v.Price
    FROM Vehicle v
	INNER JOIN Model d on v.ModelId = d.ModelId
	INNER JOIN Make k on d.MakeId = k.MakeId
	WHERE v.Featured = 1 AND NOT EXISTS(Select *
					From Sale s
					Where s.VehicleId = v.VehicleId);
GO

IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetNewVehicles')
BEGIN
   DROP PROCEDURE GetNewVehicles
END
GO
CREATE PROCEDURE GetNewVehicles
AS
    SELECT v.VehicleId, v.ModelYear, v.Picture, k.MakeDescription, d.ModelDescription, v.Price, b.BodyStyleDescription, c.ColorDescription, i.InteriorDescription, v.Featured, v.Mileage, v.MSRP, v.Transmission, v.VehicleDescription, v.VIN 
    FROM Vehicle v
	INNER JOIN Model d on v.ModelId = d.ModelId
	INNER JOIN Make k on d.MakeId = k.MakeId
	INNER JOIN BodyStyle b on b.BodyStyleId = v.BodyStyleId
	INNER JOIN Color c on c.ColorId = v.ColorId
	INNER JOIN Interior i on i.InteriorId = v.InteriorId
	WHERE v.Mileage < 1000 AND NOT EXISTS(Select *
					From Sale s
					Where s.VehicleId = v.VehicleId);
GO

IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetUsedVehicles')
BEGIN
   DROP PROCEDURE GetUsedVehicles
END
GO
CREATE PROCEDURE GetUsedVehicles
AS
    SELECT v.VehicleId, v.ModelYear, v.Picture, k.MakeDescription, d.ModelDescription, v.Price, b.BodyStyleDescription, c.ColorDescription, i.InteriorDescription, v.Featured, v.Mileage, v.MSRP, v.Transmission, v.VehicleDescription, v.VIN 
    FROM Vehicle v
	INNER JOIN Model d on v.ModelId = d.ModelId
	INNER JOIN Make k on d.MakeId = k.MakeId
	INNER JOIN BodyStyle b on b.BodyStyleId = v.BodyStyleId
	INNER JOIN Color c on c.ColorId = v.ColorId
	INNER JOIN Interior i on i.InteriorId = v.InteriorId
	WHERE v.Mileage > 1000 AND NOT EXISTS(Select *
					From Sale s
					Where s.VehicleId = v.VehicleId);
GO

IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetPurchaseVehicles')
BEGIN
   DROP PROCEDURE GetPurchaseVehicles
END
GO
CREATE PROCEDURE GetPurchaseVehicles
AS
    SELECT v.VehicleId, v.ModelYear, v.Picture, k.MakeDescription, d.ModelDescription, v.Price, b.BodyStyleDescription, c.ColorDescription, i.InteriorDescription, v.Featured, v.Mileage, v.MSRP, v.Transmission, v.VehicleDescription, v.VIN 
    FROM Vehicle v
	INNER JOIN Model d on v.ModelId = d.ModelId
	INNER JOIN Make k on d.MakeId = k.MakeId
	INNER JOIN BodyStyle b on b.BodyStyleId = v.BodyStyleId
	INNER JOIN Color c on c.ColorId = v.ColorId
	INNER JOIN Interior i on i.InteriorId = v.InteriorId
		WHERE NOT EXISTS(Select *
					From Sale s
					Where s.VehicleId = v.VehicleId)
GO


IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetVehicleById')
BEGIN
   DROP PROCEDURE GetVehicleById
END
GO
CREATE PROCEDURE GetVehicleById
	@Id int
AS
    SELECT v.VehicleId, v.ModelYear, v.Picture, k.MakeDescription, d.ModelDescription, v.Price, b.BodyStyleDescription, c.ColorDescription, i.InteriorDescription, v.Featured, v.Mileage, v.MSRP, v.Transmission, v.VehicleDescription, v.VIN 
    FROM Vehicle v
	INNER JOIN Model d on v.ModelId = d.ModelId
	INNER JOIN Make k on d.MakeId = k.MakeId
	INNER JOIN BodyStyle b on b.BodyStyleId = v.BodyStyleId
	INNER JOIN Color c on c.ColorId = v.ColorId
	INNER JOIN Interior i on i.InteriorId = v.InteriorId
	WHERE v.VehicleId = @Id;
GO

IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetUserById')
BEGIN
   DROP PROCEDURE GetUserById
END
GO
CREATE PROCEDURE GetUserById
	@Id nvarchar(100)
AS
    SELECT u.Id, u.Email, u.UserName, r.[Name]
    FROM AspNetUsers u
	INNER JOIN AspNetUserRoles a ON a.UserId = u.Id
	INNER JOIN AspNetRoles r ON r.Id = a.RoleId
	WHERE u.Id = @Id;
GO

If exists(Select * from INFORMATION_SCHEMA.ROUTINES
    Where Routine_Name = 'PostSale')
        DROP Procedure PostSale
GO
Create Procedure PostSale (
	@VehicleId int,
	@PurchaseType nvarchar(50),
	@SaleName nvarchar(75),
	@Phone nvarchar(15),
	@Email nvarchar(50),
	@Street1 nvarchar(30),
	@Street2 nvarchar(30),
	@City nvarchar(30),
	@State nvarchar(30),
	@Zipcode nvarchar(10),
	@PurchasePrice int,	
	@SalesPerson nvarchar(30)
    ) As
    Begin
    Insert Into Sale (
	VehicleId,
	PurchaseType,
	SaleName,
	Phone,
	Email,
	Street1,
	Street2,
	City,
	[State],
	Zipcode,
	PurchasePrice,	
	PurchaseDate,
	SalesPerson
)
    Values (	
	@VehicleId,
	@PurchaseType,
	@SaleName,
	@Phone,
	@Email,
	@Street1,
	@Street2,
	@City,
	@State,
	@Zipcode,
	@PurchasePrice,
	CURRENT_TIMESTAMP,
	@SalesPerson)
END
GO

If exists(Select * from INFORMATION_SCHEMA.ROUTINES
    Where Routine_Name = 'AddMake')
        DROP Procedure AddMake
GO
Create Procedure AddMake (
	@MakeDescription nvarchar(50),
	@MakeUser nvarchar(75)
    ) As
    Begin
    Insert Into Make (
	MakeDescription,
	MakeUser,
	MakeCreatedDate
)
    Values (	
	@MakeDescription,
	@MakeUser,
	CURRENT_TIMESTAMP)
END
GO

If exists(Select * from INFORMATION_SCHEMA.ROUTINES
    Where Routine_Name = 'AddModel')
        DROP Procedure AddModel
GO
Create Procedure AddModel (
	@ModelDescription nvarchar(50),
	@ModelUser nvarchar(75),
	@MakeDescription nvarchar(50)

    ) As
    Begin
    Insert Into Model (
	ModelDescription,
	ModelUser,
	ModelCreatedDate,
	MakeId
)
    Values (	
	@ModelDescription,
	@ModelUser,
	CURRENT_TIMESTAMP,
	(select MakeId From Make Where MakeDescription = @MakeDescription))
END
GO

If exists(Select * from INFORMATION_SCHEMA.ROUTINES
    Where Routine_Name = 'AddSpecial')
        DROP Procedure AddSpecial
GO
Create Procedure AddSpecial (
	@SpecialDescription nvarchar(50),
	@SpecialTitle nvarchar(75)
    ) As
    Begin
    Insert Into Special (
	SpecialDescription,
	SpecialTitle
)
    Values (	
	@SpecialDescription,
	@SpecialTitle)
END
GO

If exists(Select * from INFORMATION_SCHEMA.ROUTINES
    Where Routine_Name = 'PostContact')
        DROP Procedure PostContact
GO
Create Procedure PostContact (
	@ContactName nvarchar(75),
	@Phone nvarchar(75),
	@Email nvarchar(75),
	@Notes nvarchar(max)
    ) As
    Begin
    Insert Into Contact (
	ContactName,
	Phone,
	Email,
	Notes
)
    Values (	
	@ContactName,
	@Phone,
	@Email,
	@Notes)
END
GO


If exists(Select * from INFORMATION_SCHEMA.ROUTINES
    Where Routine_Name = 'AddVehicle')
        DROP Procedure AddVehicle
GO
Create Procedure AddVehicle (
	@Model nvarchar (140),
	@BodyStyle nvarchar (140),
	@Color nvarchar (140),
    @Description nvarchar (140),
    @Featured bit,
    @ImageUrl nvarchar (140),
    @Interior nvarchar (140),
    @Mileage int,
    @Msrp int,
    @Price int,
    @Transmission bit,
    @Vin nvarchar (140),
    @Year int
    ) As
    Begin
    Insert Into Vehicle (
	ModelId,
	BodyStyleId,
	ColorId,
	InteriorId,
	Transmission,
	ModelYear,
	Picture,
	Mileage,
	VehicleDescription,
	Featured,
	Price,
	MSRP,
	VIN
)
    Values (	
	(select ModelId From Model Where ModelDescription = @Model),
	(select BodyStyleId From BodyStyle Where BodyStyleDescription = @BodyStyle),
	(select ColorId From Color Where ColorDescription = @Color),
    (select InteriorId From Interior Where InteriorDescription = @Interior),
    @Transmission,
    @Year,
	@ImageUrl,
	@Mileage,
	@Description,
    @Featured,
    @Price,
    @Msrp,
    @Vin)
	SELECT @@IDENTITY AS VehicleId;
END
GO

If exists(Select * from INFORMATION_SCHEMA.ROUTINES
    Where Routine_Name = 'EditVehicle')
        DROP Procedure EditVehicle
GO
Create Procedure EditVehicle (
	@VehicleId int,
	@Model nvarchar (140),
	@BodyStyle nvarchar (140),
	@Color nvarchar (140),
    @Description nvarchar (140),
    @Featured bit,
    @ImageUrl nvarchar (140),
    @Interior nvarchar (140),
    @Mileage int,
    @Msrp int,
    @Price int,
    @Transmission bit,
    @Vin nvarchar (140),
    @Year int
    ) As
    Begin
    Update Vehicle SET
	ModelId = (select ModelId From Model Where ModelDescription = @Model),
	BodyStyleId = (select BodyStyleId From BodyStyle Where BodyStyleDescription = @BodyStyle),
	ColorId = (select ColorId From Color Where ColorDescription = @Color),
	InteriorId = (select InteriorId From Interior Where InteriorDescription = @Interior),
	Transmission= @Transmission,
	ModelYear = @Year,
	Picture = @ImageUrl,
	Mileage = @Mileage,
	VehicleDescription = @Description,
	Featured = @Featured,
	Price = @Price,
	MSRP = @Msrp,
	VIN = @Vin
Where VehicleId = @VehicleId;
END
GO

If exists(Select * from INFORMATION_SCHEMA.ROUTINES
    Where Routine_Name = 'DeleteVehicle')
        DROP Procedure DeleteVehicle
GO
Create Procedure DeleteVehicle (
    @VehicleId int
    ) As
    Begin
        Begin transaction
    Delete from Vehicle Where VehicleId = @VehicleId;
    Commit transaction
End
GO

If exists(Select * from INFORMATION_SCHEMA.ROUTINES
    Where Routine_Name = 'DeleteSpecial')
        DROP Procedure DeleteSpecial
GO
Create Procedure DeleteSpecial (
    @SpecialId int
    ) As
    Begin
        Begin transaction
    Delete from Special Where SpecialId = @SpecialId;
    Commit transaction
End
GO
