﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarDealership.Models
{
    public class InventoryReportVM
    {
        public List<InventoryReport> NewInventory { get; set; }
        public List<InventoryReport> UsedInventory { get; set; }
    }
}