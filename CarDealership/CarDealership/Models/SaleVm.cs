﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarDealership.Models
{
    public class SaleVm
    {
        public Vehicle Vehicle { get; set; }
        public Sale Sale { get; set; }
    }
}