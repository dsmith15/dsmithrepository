﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarDealership.Models
{
    public class HomeVM
    {
            public List<Special> SpecialList { get; set; }
            public List<FeaturedVehicle> FeaturedUiVehicles { get; set; }
    }
}