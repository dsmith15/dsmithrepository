﻿
function returnNewSearchResults() {
    $(".vehicles").remove();
    $('#errorMessages').remove();
    $.ajax({
        type: 'GET',
        url: "http://localhost:58487/newSearch"+"?makeModelYear=" + $("#makeModelYear").val() + "&minYear=" + $("#minYear").val() + "&maxYear="+$("#maxYear").val()+"&minPrice="+$("#minPrice").val()+"&maxPrice="+$("#maxPrice").val(),
        success: function (data) {
            $.each(data, function (index, item) {
                var id = item.Id;
                var year = item.Year;
                var imageUrl = item.ImageUrl;
                var make = item.Make;
                var model = item.Model;
                var price = item.Price;
                var bodystyle = item.BodyStyle;
                var color = item.Color;
                var transmission = " ";
                if (item.Transmission === false) {
                    transmission = "Automatic";
                } else {
                    transmission = "Manual";
                }
                var msrp = item.Msrp;
                var vin = item.Vin;
                var mileage = item.Mileage;
                var interior = item.Interior;
                var description = item.Description;

                var url = "/images/inventory-" + id + ".jpg";

                var result =
                    `<div class="col-md-12 vehicles">
                            <div class="col-md-3 searchColumn">
                                <p class="year">${year} ${make} ${model}</p>
                                <img class="carPic" src=${url} alt="Vehicle Picture Not Available"/> 
                            </div>
                            <div class="col-md-3">
                                <p class="bodystyle">Body Style: ${bodystyle}</p>
                                <p class="transmission">Trans: ${transmission}</p>
                                <p class="color">Color: ${color}</p>
                            </div>
                            <div class="col-md-3">
                                <p class="interior">Interior: ${interior}</p>
                                <p class="mileage">Mileage: ${mileage}</p>
                                <p class="vin">VIN #: ${vin}</p>
                            </div>
                            <div class="col-md-3">
                                <p class="price">Sale Price: $${price}</p>
                                <p class="msrp">MSRP: $${msrp}</p>
                                <button class="details" id=${this.Id}>Details</button>
                            </div>
                    </div`;
                $('#searchResults').append(result);
            });
        },
        error: function () {
            $('#errorMessages')
                .append($('<li>')
                    .attr({ class: 'list-group-item list-group-item-danger' })
                    .text('Error calling web service. Please try again later.'));
        }
    });
}

function returnUsedSearchResults() {
    $(".vehicles").remove();
    $('#errorMessages').remove();
    $.ajax({
        type: 'GET',
        url: "http://localhost:58487/usedSearch" + "?makeModelYear=" + $("#makeModelYear").val() + "&minYear=" + $("#minYear").val() + "&maxYear=" + $("#maxYear").val() + "&minPrice=" + $("#minPrice").val() + "&maxPrice=" + $("#maxPrice").val(),
        success: function (data, status) {
            $.each(data, function (index, item) {
                var id = item.Id;
                var year = item.Year;
                var imageUrl = item.ImageUrl;
                var make = item.Make;
                var model = item.Model;
                var price = item.Price;
                var bodystyle = item.BodyStyle;
                var color = item.Color;
                var transmission = " ";
                if (item.Transmission === false) {
                    transmission = "Automatic";
                } else {
                    transmission = "Manual";
                }
                var msrp = item.Msrp;
                var vin = item.Vin;
                var mileage = item.Mileage;
                var interior = item.Interior;
                var description = item.Description;

                var url = "/images/inventory-" + id + ".jpg";

                var result =
                    `<div class="col-md-12 vehicles">
                            <div class="col-md-3 searchColumn">
                                <p class="year">${year} ${make} ${model}</p>
                                <img class="carPic" src=${url} alt="Vehicle Picture Not Available"/> 
                            </div>
                            <div class="col-md-3">
                                <p class="bodystyle">Body Style: ${bodystyle}</p>
                                <p class="transmission">Trans: ${transmission}</p>
                                <p class="color">Color: ${color}</p>
                            </div>
                            <div class="col-md-3">
                                <p class="interior">Interior: ${interior}</p>
                                <p class="mileage">Mileage: ${mileage}</p>
                                <p class="vin">VIN #: ${vin}</p>
                            </div>
                            <div class="col-md-3">
                                <p class="price">Sale Price: $${price}</p>
                                <p class="msrp">MSRP: $${msrp}</p>
                                <button class="details" id=${this.Id}>Details</button>
                            </div>
                    </div`;
                $('#searchResults').append(result);
            });
        },
        error: function () {
            $('#errorMessages')
                .append($('<li>')
                    .attr({ class: 'list-group-item list-group-item-danger' })
                    .text('Error calling web service. Please try again later.'));
        }
    });
}

function returnPurchaseSearchResults() {
    $(".vehicles").remove();
    $('#errorMessages').remove();
    $.ajax({
        type: 'GET',
        url: "http://localhost:58487/purchaseSearch" + "?makeModelYear=" + $("#makeModelYear").val() + "&minYear=" + $("#minYear").val() + "&maxYear=" + $("#maxYear").val() + "&minPrice=" + $("#minPrice").val() + "&maxPrice=" + $("#maxPrice").val(),
        success: function (data, status) {
            $.each(data, function (index, item) {
                var id = item.Id;
                var year = item.Year;
                var imageUrl = item.ImageUrl;
                var make = item.Make;
                var model = item.Model;
                var price = item.Price;
                var bodystyle = item.BodyStyle;
                var color = item.Color;
                var transmission = " ";
                if (item.Transmission === false) {
                    transmission = "Automatic";
                } else {
                    transmission = "Manual";
                }
                var msrp = item.Msrp;
                var vin = item.Vin;
                var mileage = item.Mileage;
                var interior = item.Interior;
                var description = item.Description;

                var url = "/images/inventory-" + id + ".jpg";

                var result =
                    `<div class="col-md-12 vehicles">
                            <div class="col-md-3 searchColumn">
                                <p class="year">${year} ${make} ${model}</p>
                                <img class="carPic" src=${url} alt="Vehicle Picture Not Available"/> 
                            </div>
                            <div class="col-md-3">
                                <p class="bodystyle">Body Style: ${bodystyle}</p>
                                <p class="transmission">Trans: ${transmission}</p>
                                <p class="color">Color: ${color}</p>
                            </div>
                            <div class="col-md-3">
                                <p class="interior">Interior: ${interior}</p>
                                <p class="mileage">Mileage: ${mileage}</p>
                                <p class="vin">VIN #: ${vin}</p>
                            </div>
                            <div class="col-md-3">
                                <p class="price">Sale Price: $${price}</p>
                                <p class="msrp">MSRP: $${msrp}</p>
                                <button class="purchase" id=${this.Id}>Purchase</button>
                            </div>
                    </div`;
                $('#searchResults').append(result);
            });
        },
        error: function () {
            $('#errorMessages')
                .append($('<li>')
                    .attr({ class: 'list-group-item list-group-item-danger' })
                    .text('Error calling web service. Please try again later.'));
        }
    });
}

function returnAdminSearchResults() {
    $(".vehicles").remove();
    $('#errorMessages').remove();
    $.ajax({
        type: 'GET',
        url: "http://localhost:58487/adminSearch" + "?makeModelYear=" + $("#makeModelYear").val() + "&minYear=" + $("#minYear").val() + "&maxYear=" + $("#maxYear").val() + "&minPrice=" + $("#minPrice").val() + "&maxPrice=" + $("#maxPrice").val(),
        success: function (data, status) {
            $.each(data, function (index, item) {
                var id = item.Id;
                var year = item.Year;
                var imageUrl = item.ImageUrl;
                var make = item.Make;
                var model = item.Model;
                var price = item.Price;
                var bodystyle = item.BodyStyle;
                var color = item.Color;
                var transmission = " ";
                if (item.Transmission === false) {
                    transmission = "Automatic";
                } else {
                    transmission = "Manual";
                }
                var msrp = item.Msrp;
                var vin = item.Vin;
                var mileage = item.Mileage;
                var interior = item.Interior;
                var description = item.Description;

                var url = "/images/inventory-" + id + ".jpg";



                var result =
                    `<div class="col-md-12 vehicles">
                            <div class="col-md-3 searchColumn">
                                <p class="year">${year} ${make} ${model}</p>
                                <img class="carPic" src=${url} alt="Vehicle Picture Not Available"/> 
                            </div>
                            <div class="col-md-3">
                                <p class="bodystyle">Body Style: ${bodystyle}</p>
                                <p class="transmission">Trans: ${transmission}</p>
                                <p class="color">Color: ${color}</p>
                            </div>
                            <div class="col-md-3">
                                <p class="interior">Interior: ${interior}</p>
                                <p class="mileage">Mileage: ${mileage}</p>
                                <p class="vin">VIN #: ${vin}</p>
                            </div>
                            <div class="col-md-3">
                                <p class="price">Sale Price: $${price}</p>
                                <p class="msrp">MSRP: $${msrp}</p>
                                <button class="edit" id=${this.Id}>Edit</button>
                            </div>
                    </div`;
                $('#searchResults').append(result);
            });
        },
        error: function () {
            $('#errorMessages')
                .append($('<li>')
                    .attr({ class: 'list-group-item list-group-item-danger' })
                    .text('Error calling web service. Please try again later.'));
        }
    });
}

function getMakeList() {
    $(".makeDescription").remove();
    $('#errorMessages').remove();
    $.ajax({
        type: 'GET',
        url: "http://localhost:58487/makeList",
        success: function (data) {
            $.each(data, function (index, item) {
                var makeDescription = item.Description;
                var result =
                    `<option class="makeDescription" id=${makeDescription} value=${makeDescription}>${makeDescription}</option>`;
                $('#Make').append(result);
            });
            var oldMake = $("#oldMake").val();
            console.log(oldMake);
            $("#Make").val(oldMake).change();
        },
        error: function () {
            $('#errorMessages')
                .append($('<li>')
                    .attr({ class: 'list-group-item list-group-item-danger' })
                    .text('Error calling web service. Please try again later.'));
        }
    });
}

function getModelList(make) {
    $(".modelDescription").remove();
    $('#errorMessages').remove();
    $.ajax({
        type: 'GET',
        url: "http://localhost:58487/modelList" + "?make=" + make,
        success: function (data) {
            $.each(data, function (index, item) {
                var description = item.Description;
                var result =
                    `<option class="modelDescription" id=${description}>${description}</option>`;
                $('#Model').append(result);
            });

            var oldModel = $("#oldModel").val();
            console.log(oldModel);
            $("#Model").val(oldModel).change();

        },
        error: function () {
            $('#errorMessages')
                .append($('<li>')
                    .attr({ class: 'list-group-item list-group-item-danger' })
                    .text('Error calling web service. Please try again later.'));
        }
    });
}

function getUsers() {
    $(".userDescription").remove();
    $('#errorMessages').remove();
    $.ajax({
        type: 'GET',
        url: "http://localhost:58487/getUsers",
        success: function (data) {
            $.each(data, function (index, item) {
                var userDescription = item.FirstName + " " + item.LastName;
                var result =
                    `<option class="userDescription" id=${userDescription} value=${userDescription}>${userDescription}</option>`;
                $('#userSort').append(result);
            });
        },
        error: function () {
            $('#errorMessages')
                .append($('<li>')
                    .attr({ class: 'list-group-item list-group-item-danger' })
                    .text('Error calling web service. Please try again later.'));
        }
    });
}

function returnSalesReport() {
    $(".salesReport").remove();
    $('#errorMessages').remove();
    $.ajax({
        type: 'GET',
        url: "http://localhost:58487/salesReport" + "?userSort=" + $("#userSort").val() + "&startDate=" + $("#startDate").val() + "&endDate=" + $("#endDate").val(),
        success: function (data, status) {
            $.each(data, function (index, item) {
                var user = item.User;
                var totalSales = item.TotalSales;
                var totalVehicles = item.TotalVehicles;

                var result =
                    `<tr class="salesReport">
                        <td class="user">${user}</td>
                        <td class="totalSales">$${totalSales}</td>
                        <td class="totalVehicles">${totalVehicles}</td>
                    </tr>`;
                $('#searchResults').append(result);
            });
        },
        error: function () {
            $('#errorMessages')
                .append($('<li>')
                    .attr({ class: 'list-group-item list-group-item-danger' })
                    .text('Error calling web service. Please try again later.'));
        }
    });
}


$(document).ready(function () {

    $("#searchResults").hide();


    $('#newSearch').on("click",
        function(event) {
            event.preventDefault();
            $("#searchResults").show();
            returnNewSearchResults();
        });

    $('#usedSearch').on("click",
        function (event) {
            event.preventDefault();
            $("#searchResults").show();
            returnUsedSearchResults();
        });

    $('#purchaseSearch').on("click",
        function (event) {
            event.preventDefault();
            $("#searchResults").show();
            returnPurchaseSearchResults();
        });

    $('#adminSearch').on("click",
        function (event) {
            event.preventDefault();
            $("#searchResults").show();
            returnAdminSearchResults();
        });

    $(document).on("click",
        ".purchase",
        function(event) {
            event.preventDefault();
            $(".purchase").removeClass("active");
            $(this).addClass("active");

            var newId = $(this).attr("id");

            location.href = "Sales/Purchase/" + newId;
        });
    $(document).on("click",
        ".details",
        function (event) {
            event.preventDefault();
            $(".details").removeClass("active");
            $(this).addClass("active");

            var newId = this.id;

            location.href = "Details/" + newId;
        });


    $(document).on("click",
        ".edit",
        function (event) {
            event.preventDefault();
            $(".edit").removeClass("active");
            $(this).addClass("active");

            var newId = $(this).attr("id");

            location.href = "EditVehicle/" + newId;
        });

    $(document).on("click",
        "#reportSearch",
        function(event) {
            event.preventDefault();
            $("#searchResults").show();
            returnSalesReport();
        });

    getMakeList();
    getUsers();

    $(document).on("change",
        "#Make",
        function(event) {
            var make =  $("#Make").children("option").filter(":selected").text();
            getModelList(make);
        });
    //$("#Make").val("Kia");

});