﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarDealership.BLL;
using CarDealership.Models;

namespace CarDealership.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ReportsController : Controller
    {
        Manager _manager = ManagerFactory.CreateManager();

        // GET: Reports
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Sales()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Inventory()
        {
            var model = new InventoryReportVM();

            model.NewInventory = _manager.GetNewInventoryList();
            model.UsedInventory = _manager.GetUsedInventoryList();
            return View(model);
        }
    }
}