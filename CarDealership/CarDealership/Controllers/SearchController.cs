﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using CarDealership.BLL;

namespace CarDealership.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SearchController : ApiController
    {
        private Manager _manager = ManagerFactory.CreateManager();

        [Route("newSearch")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetNewVehicles(int? minYear, int? maxYear, decimal? minPrice, decimal? maxPrice, string makeModelYear)
        {
            return Ok(_manager.GetNewVehicles(minYear, maxYear, minPrice, maxPrice, makeModelYear));
        }

        [Route("usedSearch")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetUsedVehicles(int? minYear, int? maxYear, decimal? minPrice, decimal? maxPrice, string makeModelYear)
        {
            return Ok(_manager.GetUsedVehicles(minYear, maxYear, minPrice, maxPrice, makeModelYear));
        }

        [Route("purchaseSearch")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetSaleVehicles(int? minYear, int? maxYear, decimal? minPrice, decimal? maxPrice, string makeModelYear)
        {
            return Ok(_manager.GetSaleVehicles(minYear, maxYear, minPrice, maxPrice, makeModelYear));
        }

        [Route("adminSearch")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetAdminVehicles(int? minYear, int? maxYear, decimal? minPrice, decimal? maxPrice, string makeModelYear)
        {
            return Ok(_manager.GetSaleVehicles(minYear, maxYear, minPrice, maxPrice, makeModelYear));
        }

        [Route("makeList")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetMakeList()
        {
            return Ok(_manager.GetMakes());
        }

        [Route("modelList")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetModels(string make)
        {
            return Ok(_manager.GetModels(make));
        }

        [Route("getUsers")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetUsers()
        {
            return Ok(_manager.GetAllUsers());
        }

        [Route("salesReport")]
        [AcceptVerbs("GET")]
        public IHttpActionResult GetSalesReport(string userSort, string startDate, string endDate)
        {
            DateTime start = new DateTime();
            DateTime end = new DateTime();

            if (startDate == null)
            {
                start = new DateTime(1900,1,1);
            }
            else
            {
                start = DateTime.Parse(startDate);
            }

            if (endDate == null)
            {
                end = new DateTime(2100,1,1);
            }
            else
            {
                end = DateTime.Parse(endDate);
            }
            return Ok(_manager.GetSalesReports(userSort, start, end));
        }

    }
}
