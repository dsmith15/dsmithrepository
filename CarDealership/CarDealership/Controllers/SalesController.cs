﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using CarDealership.BLL;
using CarDealership.Models;
using Microsoft.ApplicationInsights.WindowsServer.Channel.Implementation;

namespace CarDealership.Controllers
{
    [Authorize(Roles = "Admin, Sales")]
    public class SalesController : Controller
    {
        private Manager _manager = ManagerFactory.CreateManager();

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Purchase(int id)
        {
            var model = new SaleVm();
            model.Vehicle = _manager.GetVehicleById(id);
            model.Sale = new Sale();
            model.Sale.VehicleId = model.Vehicle.Id;
            model.Sale.ListPrice = model.Vehicle.Price;
            return View(model);
        }

        [HttpPost]
        public ActionResult Purchase(Sale sale)
        {
            if (sale.Email == null && sale.Phone == null)
            {
                ModelState.AddModelError("Email, Phone", "Phone or Email is required.");
            }
            if (!string.IsNullOrEmpty(sale.Email))
            {
                if (Regex.IsMatch(sale.Email, @"^[a-zA-Z0-9@\.]+$") == false)
                {
                    ModelState.AddModelError("Email", "Email must be in correct format.");
                }
            }
            if (sale.Name == null)
            {
                ModelState.AddModelError("Name", "Name is required.");
            }
            if (sale.City == null)
            {
                ModelState.AddModelError("City", "City is required.");
            }
            if (sale.Zipcode.Length != 5 || Regex.IsMatch(sale.Zipcode, @"^[0-9]+$") == false)
            {
                ModelState.AddModelError("Zipcode", "Invalid Zipcode.");
            }
            if (sale.PurchasePrice < (sale.ListPrice * .95M))
            {
                ModelState.AddModelError("PurchasePrice", "Actual price cannot be less than 95% of list price.");
            }
            if (ModelState.IsValid)
            {
                sale.SalesPerson = User.Identity.Name;
                _manager.PostSale(sale);
                return RedirectToAction("Index");
            }

            var model = new SaleVm();

            model.Vehicle = _manager.GetVehicleById(sale.VehicleId);
            model.Sale = sale;

            return View(model);
        }
    }
}