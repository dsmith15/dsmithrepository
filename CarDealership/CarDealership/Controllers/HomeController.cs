﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using CarDealership.BLL;
using CarDealership.Models;

namespace CarDealership.Controllers
{
    public class HomeController : Controller
    {
        private Manager _manager = ManagerFactory.CreateManager();
        public ActionResult Index()
        {
            HomeVM model = new HomeVM();
            model.SpecialList = _manager.GetAllSpecials();
            model.FeaturedUiVehicles = _manager.GetFeaturedVehicles();
            return View(model);
        }

        public ActionResult Specials()
        {
            List<Special> model = _manager.GetAllSpecials();
            return View(model);
        }

        [HttpGet]
        public ActionResult Contact(string note)
        {
            var model = new Contact();
            model.Notes = note;

            return View(model);
        }

        [HttpPost]
        public ActionResult Contact(Contact contact)
        {
            if (contact.Email == null && contact.Phone == null)
            {
                ModelState.AddModelError("Email", "Phone or Email is required.");
            }
            if (contact.Name == null)
            {
                ModelState.AddModelError("Name", "Name is required.");
            }
            if (contact.Notes == null)
            {
                ModelState.AddModelError("Notes", "Notes is required.");
            }
            if (ModelState.IsValid)
            {
                _manager.PostContact(contact);
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}