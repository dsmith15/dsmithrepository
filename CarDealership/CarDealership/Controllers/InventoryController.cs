﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using CarDealership.BLL;
using CarDealership.Models;

namespace CarDealership.Controllers
{
    public class InventoryController : Controller
    {
        private Manager _manager = ManagerFactory.CreateManager();

        [HttpGet]
        public ActionResult New()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Used()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            return View(_manager.GetVehicleById(id));
        }
    }
}