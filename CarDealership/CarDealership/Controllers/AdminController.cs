﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CarDealership.BLL;
using CarDealership.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace CarDealership.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AdminController()
        {
        }

        public AdminController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        Manager _manager = ManagerFactory.CreateManager();
        [HttpGet]
        public ActionResult Vehicles()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddVehicle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddVehicle(Vehicle vehicle, HttpPostedFileBase file)
        {
            
            if (Regex.IsMatch(vehicle.Vin, @"^[A-Z0-9]+$") == false || vehicle.Vin.Length != 17)
            {
                ModelState.AddModelError("Vin", "Invalid VIN");
            }
            if (vehicle.Year < 2000 || vehicle.Year > (DateTime.Now.Year + 1))
            {
                ModelState.AddModelError("Year", "Invalid Year. Model Year must be 2000 or later.");
            }
            if (vehicle.Description == null)
            {
                ModelState.AddModelError("Description", "Vehicle Description is required.");
            }
            if (vehicle.Msrp <= 0 || vehicle.Price <= 0)
            {
                ModelState.AddModelError("Msrp", "MSRP & Price must be greater than 0.");
            }
            if (vehicle.Price >= vehicle.Msrp)
            {
                ModelState.AddModelError("Price", "Price must be less than MSRP.");
            }
            if (vehicle.Model == null)
            {
                ModelState.AddModelError("Model", "Model is a required field.");
            }
            if (file == null)
            {
                ModelState.AddModelError("blah", "Picture is required.");
            }
            if (ModelState.IsValid)
            {
                int newId = _manager.CreateVehicle(vehicle);

                string filename = "inventory-" + newId + ".jpg";

                string _path = Path.Combine(Server.MapPath("~/images"), filename);

                if (!System.IO.File.Exists(_path))
                {
                    System.IO.File.Create(_path).Close();
                }
                file.SaveAs(_path);
                return RedirectToAction("EditVehicle", new { id = newId });
            }
            return View();
        }

        [HttpGet]
        public ActionResult EditVehicle(int id)
        {
            var model = _manager.GetVehicleById(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult EditVehicle(Vehicle vehicle, HttpPostedFileBase file)
        {
            if (Regex.IsMatch(vehicle.Vin, @"^[A-Z0-9]+$") == false || vehicle.Vin.Length != 17)
            {
                ModelState.AddModelError("Vin", "Invalid VIN");
            }
            if (vehicle.Year < 2000 || vehicle.Year > (DateTime.Now.Year + 1))
            {
                ModelState.AddModelError("Year", "Invalid Year. Model Year must be 2000 or later.");
            }
            if (vehicle.Description == null)
            {
                ModelState.AddModelError("Description", "Vehicle Description is required.");
            }
            if (vehicle.Msrp <= 0 || vehicle.Price <= 0)
            {
                ModelState.AddModelError("Msrp", "MSRP & Price must be greater than 0.");
            }
            if (vehicle.Price >= vehicle.Msrp)
            {
                ModelState.AddModelError("Price", "Price must be less than MSRP.");
            }
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    string filename = "inventory-" + vehicle.Id + ".jpg";

                    string _path = Path.Combine(Server.MapPath("~/images"), filename);

                    if (System.IO.File.Exists(_path))
                    {
                        System.IO.File.Delete(_path);
                    }

                    if (!System.IO.File.Exists(_path))
                    {
                        System.IO.File.Create(_path).Close();
                    }
                    file.SaveAs(_path);
                }
                _manager.EditVehicle(vehicle);
                return RedirectToAction("Vehicles");
            }
            return View();
        }
        [HttpGet]
        public ActionResult DeleteVehicle(int id)
        {
            var model = _manager.GetVehicleById(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteVehiclePost(int id)
        {
            string filename = "inventory-" + id + ".jpg";

            string _path = Path.Combine(Server.MapPath("~/images"), filename);

            if (System.IO.File.Exists(_path))
            {
                System.IO.File.Delete(_path);
            }

            _manager.DeleteVehicle(id);
            return RedirectToAction("Vehicles");
        }

        [HttpGet]
        public ActionResult Users()
        {
            var model = _manager.GetAllUsers();
            return View(model);
        }

        [HttpGet]
        public ActionResult AddUser()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddUser(User user, string confirmPassword)
        {
            if (string.IsNullOrEmpty(user.FirstName))
            {
                ModelState.AddModelError("FirstName", "First Name is a required field.");
            }
            if (string.IsNullOrEmpty(user.LastName))
            {
                ModelState.AddModelError("LastName", "Last Name is a required field.");
            }
            if (string.IsNullOrEmpty(user.Email))
            {
                ModelState.AddModelError("Email", "Email is a required field.");
            }
            if (string.IsNullOrEmpty(user.Password))
            {
                ModelState.AddModelError("Password", "Password is a required field.");
            }
            if (user.Password != confirmPassword)
            {
                ModelState.AddModelError("Password", "Password & Confirm Password must match.");
            }
            if (ModelState.IsValid)
            {
                var newUser = new ApplicationUser();
                newUser.Email = user.Email;
                newUser.UserName = user.FirstName + " " + user.LastName;

                var chkUser = await UserManager.CreateAsync(newUser, user.Password);

                if (chkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(newUser.Id, user.Role);
                }
                var model = _manager.GetAllUsers();
                return View("Users", model);
            }
            return View();
        }

        [HttpGet]
        public ActionResult EditUser(string id)
        {
            var model = _manager.GetUserById(id);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EditUser(User user, string confirmPassword)
        {
            if (string.IsNullOrEmpty(user.FirstName))
            {
                ModelState.AddModelError("FirstName", "First Name is a required field.");
            }
            if (string.IsNullOrEmpty(user.LastName))
            {
                ModelState.AddModelError("LastName", "Last Name is a required field.");
            }
            if (string.IsNullOrEmpty(user.Email))
            {
                ModelState.AddModelError("Email", "Email is a required field.");
            }
            if (user.Password != confirmPassword)
            {
                ModelState.AddModelError("Password", "Password & Confirm Password must match.");
            }
            if (ModelState.IsValid)
            {
                var newUser = UserManager.FindById(user.Id);
                newUser.Email = user.Email;
                newUser.UserName = user.FirstName + " " + user.LastName;

                var chkUser = await UserManager.UpdateAsync(newUser);

                if (chkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(newUser.Id, user.Role);
                }
                var model = _manager.GetAllUsers();
                return View("Users", model);
            }
            return View();
        }

        [HttpGet]
        public ActionResult Makes()
        {
            var model = _manager.GetMakes();
            return View(model);
        }

        [HttpPost]
        public ActionResult Makes(string description)
        {
            if (string.IsNullOrEmpty(description))
            {
                ModelState.AddModelError("description", "New Model is a required field.");
            }

            if (ModelState.IsValid)
            {
                Make make = new Make();
                make.Description = description;
                make.User = User.Identity.Name;
                _manager.AddMake(make);
            }
            var model = _manager.GetMakes();
            return View("Makes", model);
        }

        [HttpGet]
        public ActionResult Models()
        {
            var model = _manager.GetAllModels();
            return View(model);
        }

        [HttpPost]
        public ActionResult Models(string description, string make)
        {
            if (string.IsNullOrEmpty(description))
            {
                ModelState.AddModelError("description", "New Model is a required field.");
            }

            if (ModelState.IsValid)
            {
                Model newModel = new Model();
                newModel.Description = description;
                newModel.MakeDescription = make;
                newModel.User = User.Identity.Name;
                _manager.AddModel(newModel);
            }
            var model = _manager.GetAllModels();
            return View("Models", model);
        }

        [HttpGet]
        public ActionResult Specials()
        {
            var model = _manager.GetAllSpecials();
            return View(model);
        }

        [HttpPost]
        public ActionResult Specials(string title, string description)
        {
            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(description))
            {
                ModelState.AddModelError("title","Title & Description are required fields.");
            }

            if (ModelState.IsValid)
            {
                Special special = new Special();
                special.Description = description;
                special.Title = title;
                _manager.AddSpecial(special);
            }

            var model = _manager.GetAllSpecials();

            return View("Specials", model);
        }

        [HttpGet]
        public ActionResult DeleteSpecial(int id)
        {
            var model = _manager.GetSpecialById(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteSpecialPost(int id)
        {
            _manager.DeleteSpecial(id);
            return RedirectToAction("Specials");
        }
    }
}