﻿Use GuildCars
Go

If Exists(Select * From sys.tables Where Name = 'Special')
Drop Table Special
Go

If Exists(Select * From sys.tables Where Name = 'Sale')
Drop Table Sale
Go

If Exists(Select * From sys.tables Where Name = 'Vehicle')
Drop Table Vehicle
Go

If Exists(Select * From sys.tables Where Name = 'Model')
Drop Table Model
Go

If Exists(Select * From sys.tables Where Name = 'Contact')
Drop Table Contact
Go

If Exists(Select * From sys.tables Where Name = 'Make')
Drop Table Make
Go

If Exists(Select * From sys.tables Where Name = 'Interior')
Drop Table Interior
Go

If Exists(Select * From sys.tables Where Name = 'BodyStyle')
Drop Table BodyStyle
Go

If Exists(Select * From sys.tables Where Name = 'Color')
Drop Table Color
Go



Create Table Make(
	MakeId int identity(1,1) not null primary key,
	MakeDescription nvarchar(25) not null,
	MakeUser nvarchar(50) not null,
	MakeCreatedDate DateTime2 not null
)
Go

Create Table Interior(
	InteriorId int identity(1,1) not null primary key,
	InteriorDescription nvarchar(140) not null
)
Go

Create Table BodyStyle(
	BodyStyleId int identity(1,1) not null primary key,
	BodyStyleDescription nvarchar(25) not null
)
Go

Create Table Color(
	ColorId int identity(1,1) not null primary key,
	ColorDescription nvarchar(25) not null
)
Go

Create Table Contact(
	ContactId int identity(1,1) not null primary key,
	ContactName nvarchar(75) not null,
	Phone nvarchar(15) null,
	Email nvarchar(50) null,
	Notes nvarchar(max) not null,
)
Go

Create Table Model(
	ModelId int identity(1,1) not null primary key,
	ModelDescription nvarchar(20) not null,
	ModelUser nvarchar(50) not null,
	ModelCreatedDate DateTime2 not null,
	MakeId int foreign key references Make(MakeId) not null
)
Go

Create Table Vehicle(
	VehicleId int identity(1,1) not null primary key,
	ModelId int foreign key references Model(ModelId) not null,
	BodyStyleId int foreign key references BodyStyle(BodyStyleId) not null,
	ColorId int foreign key references Color(ColorId) not null,
	InteriorId int foreign key references Interior(InteriorId) not null,
	Transmission bit not null,
	ModelYear int not null,
	Picture nvarchar(max) not null,
	Mileage int not null,
	VehicleDescription nvarchar(max) not null,
	Featured bit not null,
	Price int not null,
	MSRP int not null,
	VIN varchar(17)
)
Go

Create Table Sale(
	SaleId int identity(1,1) not null primary key,
	VehicleId int foreign key references Vehicle(VehicleId) not null,
	PurchaseType nvarchar(50) not null,
	SaleName nvarchar(75) not null,
	Phone nvarchar(15) null,
	Email nvarchar(50) null,
	Street1 nvarchar(30) not null,
	Street2 nvarchar(30) null,
	City nvarchar(30) not null,
	[State] nvarchar(30) not null,
	Zipcode nvarchar(10) not null,
	PurchasePrice int not null,	
	PurchaseDate datetime2 not null,
	SalesPerson nvarchar(30) not null
)
Go

Create Table Special(
	SpecialId int identity(1,1) not null primary key,
	SpecialTitle nvarchar(140) not null,
	SpecialDescription nvarchar(max) not null,
)
Go
