﻿USE GuildCars
GO
IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'NewInventoryReport')
BEGIN
   DROP PROCEDURE NewInventoryReport
END
GO
CREATE PROCEDURE NewInventoryReport
AS
    SELECT Count(*) AS TotalVehicles, SUM(v.MSRP) as TotalValue, v.ModelYear, k.MakeDescription, m.ModelDescription
    FROM Vehicle v
	INNER JOIN Model m on m.ModelId = v.ModelId
	INNER JOIN Make k on k.MakeId = m.MakeId
	WHERE v.Mileage < 1000 AND NOT EXISTS(Select *
					From Sale s
					Where s.VehicleId = v.VehicleId)
	GROUP BY k.MakeDescription, m.ModelDescription, v.ModelYear
GO

IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'OldInventoryReport')
BEGIN
   DROP PROCEDURE OldInventoryReport
END
GO
CREATE PROCEDURE OldInventoryReport
AS
    SELECT Count(*) AS TotalVehicles, SUM(v.MSRP) as TotalValue, v.ModelYear, k.MakeDescription, m.ModelDescription
    FROM Vehicle v
	INNER JOIN Model m on m.ModelId = v.ModelId
	INNER JOIN Make k on k.MakeId = m.MakeId
	WHERE v.Mileage > 1000 AND NOT EXISTS(Select *
					From Sale s
					Where s.VehicleId = v.VehicleId)
	GROUP BY k.MakeDescription, m.ModelDescription, v.ModelYear
GO

IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'SalesReport')
BEGIN
   DROP PROCEDURE SalesReport
END
GO
CREATE PROCEDURE SalesReport
	@User nvarchar (100),
	@StartDate DateTime,
	@EndDate DateTime
AS
    SELECT Count(*) AS TotalVehicles, SUM(s.PurchasePrice) as TotalValue, s.SalesPerson
    FROM Sale s
	WHERE s.PurchaseDate BETWEEN @StartDate AND @EndDate AND s.SalesPerson = ISNULL(@User, s.SalesPerson)
	GROUP BY s.SalesPerson
GO

 