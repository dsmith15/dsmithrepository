﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarDealership.DAL;
using CarDealership.Models;
using NUnit.Framework;

namespace CarDealership.Tests
{
    [TestFixture]
    public class Tests
    {
        private string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        
        [Test]
        public void MakeRepoTest()
        {
            MakeADORepository repo = new MakeADORepository();

            var startList = repo.GetAll();

            var actual = new Make()
            {
                Description = "Test",
                User = "test"
            };

            repo.AddMake(actual);

            var endlist = repo.GetAll();

            Assert.AreEqual(endlist.Count, startList.Count + 1);

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Exec DBReset", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
            }
        }

        [TestCase("Toyota")]
        [TestCase("Ford")]
        public void ModelRepoGetByMakeTest(string make)
        {
            ModelADORepository repo = new ModelADORepository();

            var actual = repo.GetAllByMake(make);

            Assert.AreEqual(3, actual.Count);

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Exec DBReset", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        [Test]
        public void ModelRepoTest()
        {
            ModelADORepository repo = new ModelADORepository();

            var startList = repo.GetAll();

            var actualModel = new Model()
            {
                Description = "Test",
                MakeDescription = "Toyota",
                User = "Test"
            };

            repo.AddModel(actualModel);

            var endList = repo.GetAll();


            Assert.AreEqual(endList.Count, startList.Count + 1);

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Exec DBReset", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        [Test]
        public void TestSpecialRepo()
        {
            SpecialADORepository repo = new SpecialADORepository();

            var firstList = repo.GetAll();

            var actualspecial = new Special()
            {
                Title = "Test",
                Description = "Test"
            };

            repo.AddSpecial(actualspecial);

            var secondList = repo.GetAll();

            repo.DeleteSpecial(secondList.Last().Id);

            var thirdList = repo.GetAll();

            Assert.AreEqual(secondList.Count, firstList.Count +1);
            Assert.AreEqual(secondList.Count, thirdList.Count + 1);

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Exec DBReset", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        [Test]
        public void ContactTest()
        {
            ContactADORepository repo = new ContactADORepository();

            var actual = new Contact()
            {
                Name = "Test",
                Email = "Test",
                Notes = "Test",
                Phone = "test"
            };

            repo.PostContact(actual);

            int total = 0;

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Select Count(*) As total From Contact", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        total = int.Parse(dr["total"].ToString());
                    }
                }
                cn.Close();
            }

            Assert.AreEqual(4, total);
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Exec DBReset", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        [Test]
        public void TestSale()
        {
            SaleADORepository repo = new SaleADORepository();

            var actual = new Sale()
            {
                City = "Test",
                Email = "Test",
                ListPrice = 100,
                Name = "Test",
                Phone = "Test",
                PurchasePrice = 100,
                PurchaseType = "Test",
                SalesPerson = "Test",
                Street1 = "Test",
                State = "Test",
                VehicleId = 2,
                Zipcode = "44212"
            };

            repo.CreateSale(actual);

            int total = 0;

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Select Count(*) As total From Sale", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        total = int.Parse(dr["total"].ToString());
                    }
                }
                cn.Close();
            }

            Assert.AreEqual(12, total);
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Exec DBReset", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        [Test]
        public void GetNewInventoryReportTest()
        {
            ReportADORepository repo = new ReportADORepository();

            var newList = repo.GetNewInventory();
            int total = 0;

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Select Count(*) As total From Vehicle Where Vehicle.Mileage < 1000 AND NOT EXISTS (Select * From Sale Where Sale.VehicleId = Vehicle.VehicleId)", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        total = int.Parse(dr["total"].ToString());
                    }
                }
                cn.Close();
            }

            Assert.AreEqual(total, newList.Count);
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Exec DBReset", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }

        }

        [Test]
        public void GetUsedInventoryReportTest()
        {
            ReportADORepository repo = new ReportADORepository();

            var newList = repo.GetUsedInventory();
            int total = 0;

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Select Count(*) As total From Vehicle Where Vehicle.Mileage > 1000 AND NOT EXISTS (Select * From Sale Where Sale.VehicleId = Vehicle.VehicleId)", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        total = int.Parse(dr["total"].ToString());
                    }
                }
                cn.Close();
            }

            Assert.AreEqual(total, newList.Count);
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Exec DBReset", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }

        }

        [Test]
        public void FeaturedVehicleTest()
        {
            VehicleADORepository repo = new VehicleADORepository();

            var list = repo.GetFeaturedVehicles();

            int total = 0;

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd =
                    new SqlCommand(
                        "Select Count(*) As total From Vehicle v WHERE v.Featured = 1 AND NOT EXISTS(Select * From Sale s Where s.VehicleId = v.VehicleId)", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        total = int.Parse(dr["total"].ToString());
                    }
                }
                cn.Close();
            }

            Assert.AreEqual(total, list.Count);
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Exec DBReset", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        [Test]
        public void NewVehicleTest()
        {
            VehicleADORepository repo = new VehicleADORepository();

            var list = repo.GetNewVehicles();

            int total = 0;

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd =
                    new SqlCommand(
                        "Select Count(*) As total From Vehicle v WHERE v.Mileage < 1000 AND NOT EXISTS(Select * From Sale s Where s.VehicleId = v.VehicleId)", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        total = int.Parse(dr["total"].ToString());
                    }
                }
                cn.Close();
            }

            Assert.AreEqual(total, list.Count);
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Exec DBReset", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }
        [Test]
        public void UsedVehicleTest()
        {
            VehicleADORepository repo = new VehicleADORepository();

            var list = repo.GetUsedVehicles();

            int total = 0;

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd =
                    new SqlCommand(
                        "Select Count(*) As total From Vehicle v WHERE v.Mileage > 1000 AND NOT EXISTS(Select * From Sale s Where s.VehicleId = v.VehicleId)", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        total = int.Parse(dr["total"].ToString());
                    }
                }
                cn.Close();
            }

            Assert.AreEqual(total, list.Count);
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Exec DBReset", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }
        [Test]
        public void SaleVehicleTest()
        {
            VehicleADORepository repo = new VehicleADORepository();

            var list = repo.GetSaleVehicles();

            int total = 0;

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd =
                    new SqlCommand(
                        "Select Count(*) As total From Vehicle v WHERE NOT EXISTS(Select * From Sale s Where s.VehicleId = v.VehicleId)", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        total = int.Parse(dr["total"].ToString());
                    }
                }
                cn.Close();
            }

            Assert.AreEqual(total, list.Count);
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("Exec DBReset", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        [Test]
        public void VehicleCrudTest()
        {
                VehicleADORepository repo = new VehicleADORepository();

                var actual = new Vehicle()
                {
                    BodyStyle = "Test",
                    Color = "Test",
                    Year = 2017,
                    Model = "Test",
                    Msrp = 15200,
                    Make = "Test",
                    Mileage = 15632,
                    Description = "Test",
                    Price = 14000,
                    Transmission = false,
                    Vin = "Test",
                    Interior = "Test"
                };

            int number = repo.AddVehicle(actual);

            var expected = repo.GetById(number);

            expected.Model = "Toyota";

            repo.EditVehicle(expected, number);

            Assert.AreEqual(expected.Model, "Toyota");

            var test = repo.GetById(number);

            repo.DeleteVehicle(number);

                using (var cn = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand("Exec DBReset", cn);
                    cmd.CommandType = System.Data.CommandType.Text;
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }

    }

}
}
