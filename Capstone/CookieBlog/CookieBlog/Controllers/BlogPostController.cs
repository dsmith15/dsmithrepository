﻿using CookieBlog.BLL;
using CookieBlog.Models;
using CookieBlog.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;
using System.Windows.Forms;

namespace CookieBlog.Controllers
{

    public class BlogPostController : Controller
    {
        private Manager _manager = ManagerFactory.CreateManager();

        [HttpGet]
        public ActionResult Index()
        {
            return RedirectToAction("Home");
        }

        [HttpGet]
        public ActionResult Home()
        {
            var model = _manager.GetAllBlogPostTagsVm();

            return View(model);
        }

        [HttpPost]
        public ActionResult Home(BlogPostTagsVM blogTagVM)
        {
            return View();
        }

        [HttpGet]
        public ActionResult Details(int blogid)
        {
            BlogPost model = _manager.GetAnyBlogPost(blogid);
            return View(model);
        }

        [HttpPost]
        public ActionResult Details(BlogPost PostVM)
        {
            return View();
        }

        [HttpGet]
        [Authorize(Roles="Admin, Contributor")]
        public ActionResult Add()
        {
            CreateBlogPostVM model = new CreateBlogPostVM()
            {
                blogPost = new BlogPost(),
                categoryList = _manager.GetAllCategories(),
            };

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Contributor")]
        public ActionResult Add(CreateBlogPostVM model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    string filename = "Image" + DateTime.Now.ToString("yy-MM-dd-hh-mm") + ".jpg";

                    string _path = Path.Combine(Server.MapPath("~/uploads"), filename);

                    if (!System.IO.File.Exists(_path))
                    {
                        System.IO.File.Create(_path).Close();
                    }

                    file.SaveAs(_path);

                    model.blogPost.BlogImage = "/uploads/" + filename;
                }
                model.blogPost.Author = User.Identity.Name;
                BlogPost sampleBlogPost = _manager.CreateNewBlogPost(model.blogPost);
                return RedirectToAction("Details", new { blogid = sampleBlogPost.BlogId });
            }
            else
            {
                return View("Add", model);
            }
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
            BlogPost model = _manager.GetAnyBlogPost(id);
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(BlogPost model)
        {
            string filename = model.BlogImage;

            if(filename != null)
            {
                string _path = Path.Combine(Server.MapPath("~/uploads"), filename);

                if (System.IO.File.Exists(_path))
                {
                    System.IO.File.Delete(_path);
                }
            }

            _manager.Delete(model.BlogId);
            return RedirectToAction("Home");
        }

        [HttpGet]
        [Authorize(Roles = "Admin, Contributor")]
        public ActionResult Edit(int blogid)
        {
            CreateBlogPostVM model = new CreateBlogPostVM()
            {
                blogPost = _manager.GetAnyBlogPost(blogid),
                categoryList = _manager.GetAllCategories(),
            };
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Contributor")]
        public ActionResult Edit(CreateBlogPostVM model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {

                if (file != null)
                {
                    string filename = "Image" + DateTime.Now.ToString("yy-MM-dd-hh-mm") + ".jpg";

                    string _path = Path.Combine(Server.MapPath("~/uploads"), filename);

                    if (System.IO.File.Exists(model.blogPost.BlogImage))
                    {
                        System.IO.File.Delete(model.blogPost.BlogImage);
                    }
                    if (!System.IO.File.Exists(_path))
                    {
                        System.IO.File.Create(_path).Close();
                    }
                    file.SaveAs(_path);

                    model.blogPost.BlogImage = "/uploads/" + filename;
                }

                if (User.IsInRole("Contributor"))
                {
                    model.blogPost.IsApproved = false;
                }
                BlogPost sampleBlogPost = _manager.UpdateBlogPost(model.blogPost);
                return RedirectToAction("Details", new { blogid = sampleBlogPost.BlogId });
            }
            else
            {
                return View("Edit", model);
            }
        }

        [HttpGet]
        public ActionResult SearchResults(string searchText, string search)
        {
            List<BlogPost> model = new List<BlogPost>();
            List<BlogPost> blogPostList = _manager.GetAllBlogPost();

            if (search == "Category")
            {
                foreach (var item in blogPostList)
                {
                    if (item.Category.Contains(searchText))
                    {
                        model.Add(item);
                    }
                }
            }
            else if (search == "Title")
            {
                foreach (var item in blogPostList)
                {
                    if (item.BlogTitle.Contains(searchText))
                    {
                        model.Add(item);
                    }
                }
            }
            else
            {
                foreach (var item in blogPostList)
                {
                    if (item.BlogTags.Contains(searchText))
                    {
                        model.Add(item);
                    }
                }
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult UnApprovedBlogs()
        {
            var model = _manager.GetAllBlogPost();
            return View(model);
        }
    }
}