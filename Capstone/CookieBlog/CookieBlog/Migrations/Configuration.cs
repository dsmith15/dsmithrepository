using CookieBlog.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CookieBlog.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CookieBlog.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "CookieBlog.Models.ApplicationDbContext";
        }

        //protected override void Seed(CookieBlog.Models.ApplicationDbContext context)
        //{
        //    var userMgr = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
        //    var roleMgr = new RoleManager<AppRole>(new RoleStore<AppRole>(context));

        //    if (roleMgr.RoleExists("admin"))
        //    {
        //        return;
        //    }

        //    roleMgr.Create(new AppRole() { Name = "admin" });

        //    userMgr.AddToRole("1b69c00a - 0292 - 4b87 - 991c - bf1b27bdbfaf", "admin");
        //    //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
        //    //  to avoid creating duplicate seed data.
        //}
    }
}
