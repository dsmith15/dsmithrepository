﻿var App = {
    jumboBlogs: [],
    titleList: []
}

$("#titleSearch").click(function () {
    console.log("you can click this!");
})

$("#featured").click(function () {
    tagz = document.getElementById('tagz')
    tagz.value += "#" + this.value;
    console.log(this.value);
});

$(".tag").click(function () {
    tagz = document.getElementById('tagz')
    tagz.value += "#" + this.value;
    console.log(this.value);
});

$("#searchButton").click(function () {
    var searchType = "";
    if ($("#search").val() === "Search By Title") {
        searchType = "Title";
        console.log(searchType);
    }
    if ($("#search").val() === "Search By Category") {
        searchType = "Category";
        console.log(searchType);
    }
    if ($("#search").val() === "Search By Tag") {
        searchType = "Tag";
        console.log(searchType);
    }

    console.log("Clicked Search Button");
    var searchWord = $("#whiteSpace").val();
    console.log(searchWord);
    var searchUrl = 'SearchResults';
    searchUrl += "?searchText=" + searchWord +'&search='+ searchType;
    window.location.href = searchUrl;
})

var page = {
    itemCount: 10,
    pageCount: 1,
    totalItems: 0,
};

function BlogPostPager() {
    totalItems = $(".postTable").length;
    $(".postTable").each(function (item, index) {
        if (this.id > ((page.itemCount * page.pageCount)) || this.id < ((page.itemCount * (page.pageCount - 1)))) {
            this.style.display = "none";
        }
        else
        {
            this.style.display = "block";
        }
    });
}

function PageNavConfig() {
    if (page.pageCount > 1) {
        $("#prevPage").show();
    }
    else {
        $("#prevPage").hide();
    }
    if (((page.pageCount * page.itemCount)) >= totalItems) {
        $("#nextPage").hide();
    }
    else {
        $("#nextPage").show();
    }
}

function PagerConfig() {
    BlogPostPager();
    PageNavConfig();
}


$(document).ready(function () {
    page.pageCount = 1;
    PagerConfig();

    $("#prevPage").click(function() {
        page.pageCount--;
        PagerConfig();
    });
    $("#nextPage").click(function() {
        page.pageCount++;
        PagerConfig();
    });

    $(document).on("click", "#logoffButton", function() {
        $.post("/Account/LogOff");
        window.location = "Home";
    });

    
})