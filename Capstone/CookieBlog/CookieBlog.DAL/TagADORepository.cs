﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CookieBlog.Models;
using System.Configuration;
using System.Data.SqlClient;
namespace CookieBlog.DAL
{
    public class TagADORepository : ITagRepository
    {
        private string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public void Create(Tag tag)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("CreateTags", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TagName", tag.TagName);
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }
        public void Delete(int id)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd1 = new SqlCommand("DeletePostTagsByTag", cn);
                cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@TagId", id);
                cn.Open();
                cmd1.ExecuteNonQuery();
                cn.Close();

                SqlCommand cmd2 = new SqlCommand("DeleteTags", cn);
                cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@TagId", id);
                cn.Open();
                cmd2.ExecuteNonQuery();
                cn.Close();
            }
        }
        public List<Tag> GetAll()
        {
            List<Tag> list = new List<Tag>();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetAllTags", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Tag currentRow = new Tag();
                        currentRow.TagId = int.Parse(dr["TagId"].ToString());
                        currentRow.TagName = dr["TagName"].ToString();
                        list.Add(currentRow);
                    }
                }
            }
            return list;
        }
        public Tag GetById(int id)
        {
            Tag currentRow = new Tag();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetTagsById", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TagId", id);
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        currentRow.TagId = int.Parse(dr["TagId"].ToString());
                        currentRow.TagName = dr["TagName"].ToString();
                    }
                }
                return currentRow;
            }
        }
        public void Update(Tag Tag, int id)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("UpdateTags", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TagId", Tag.TagId);
                cmd.Parameters.AddWithValue("@TagName", Tag.TagName);
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        public Tag GetByName(string tag)
        {
            Tag currentRow = new Tag();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetTagsByName", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TagName", tag);
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        currentRow.TagId = int.Parse(dr["TagId"].ToString());
                        currentRow.TagName = dr["TagName"].ToString();
                    }
                }
            }
            return currentRow;
        }
    }
}
