﻿using CookieBlog.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace CookieBlog.DAL
{
    public class BlogPostADORepository : IBlogPostRepository
    {
        private string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public int Create(BlogPost blogpost)
        {
            int id = 0;
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("CreateBlogPost", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BlogTitle", blogpost.BlogTitle);
                cmd.Parameters.AddWithValue("@BlogContent", blogpost.BlogContent);
                cmd.Parameters.AddWithValue("@BlogIsApproved", blogpost.IsApproved);
                cmd.Parameters.AddWithValue("@BlogStartDate", blogpost.BlogStartDate);
                if (blogpost.BlogEndDate == null)
                {
                    cmd.Parameters.AddWithValue("@BlogEndDate", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@BlogEndDate", blogpost.BlogEndDate);
                }
                if (string.IsNullOrEmpty(blogpost.Category))
                {
                    blogpost.Category = "Uncategorized";
                }
                cmd.Parameters.AddWithValue("@CategoryName", blogpost.Category);
                cmd.Parameters.AddWithValue("@IsFeatured", blogpost.IsFeatured);
                cmd.Parameters.AddWithValue("@IsStatic", blogpost.IsStatic);
                cmd.Parameters.AddWithValue("@Author", blogpost.Author);
                if (blogpost.BlogTags == null)
                {
                    cmd.Parameters.AddWithValue("@TagString", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@TagString", blogpost.BlogTags);
                }

                if (blogpost.BlogImage == null)
                {
                    cmd.Parameters.AddWithValue("@ImageFileUrl", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ImageFileUrl", blogpost.BlogImage);
                }
                try
                {
                    cn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        id = int.Parse(dr["BlogId"].ToString());
                    }
                }
                catch (Exception e)
                {
                    string error = e.Message;
                }
                return id;
            }
        }
        public void Delete(int id)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("DeleteBlogPost", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BlogId", id);
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }
        public List<BlogPost> GetAll()
        {
            List<BlogPost> list = new List<BlogPost>();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetAllBlogPosts", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        list.Add(Mapper.MapBlogPost(dr));
                    }
                }
            }
            return list;
        }
        public List<BlogPost> GetMainBlogList()
        {
            List<BlogPost> list = new List<BlogPost>();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetMainBlogList", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        list.Add(Mapper.MapBlogPost(dr));
                    }
                }
            }
            return list;
        }
        public BlogPost GetById(int id)
        {
            BlogPost currentRow = new BlogPost();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetBlogPostsById", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BlogId", id);
                cn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while(dr.Read())
                {
                    currentRow = Mapper.MapBlogPost(dr);
                }
            }
            return currentRow;
        }
        public void Update(BlogPost blogpost, int id)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("UpdateBlogPost", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BlogID", blogpost.BlogId);
                cmd.Parameters.AddWithValue("@BlogTitle", blogpost.BlogTitle);
                cmd.Parameters.AddWithValue("@BlogContent", blogpost.BlogContent);
                cmd.Parameters.AddWithValue("@BlogIsApproved", blogpost.IsApproved);
                cmd.Parameters.AddWithValue("@BlogStartDate", blogpost.BlogStartDate);
                if (blogpost.BlogEndDate == null)
                {
                    cmd.Parameters.AddWithValue("@BlogEndDate", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@BlogEndDate", blogpost.BlogEndDate);
                }
                if (string.IsNullOrEmpty(blogpost.Category))
                {
                    blogpost.Category = "Uncategorized";
                }
                cmd.Parameters.AddWithValue("@CategoryName", blogpost.Category);
                cmd.Parameters.AddWithValue("@IsFeatured", blogpost.IsFeatured);
                cmd.Parameters.AddWithValue("@IsStatic", blogpost.IsStatic);
                cmd.Parameters.AddWithValue("@Author", blogpost.Author);
                if (blogpost.BlogTags == null)
                {
                    cmd.Parameters.AddWithValue("@TagString", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@TagString", blogpost.BlogTags);
                }
                if (blogpost.BlogImage == null)
                {
                    cmd.Parameters.AddWithValue("@ImageFileUrl", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ImageFileUrl", blogpost.BlogImage);
                }
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }
        public List<BlogPost> GetByTag(string tag)
        {
            List<BlogPost> list = new List<BlogPost>();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetBlogPostsByTagName", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TagName", tag);
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        list.Add(Mapper.MapBlogPost(dr));
                    }
                }
                return list;
            }
        }
        public List<BlogPost> GetByTitle(string title)
        {
            List<BlogPost> list = new List<BlogPost>();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetBlogPostsByTitle", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BlogTitle", title);
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        list.Add(Mapper.MapBlogPost(dr));
                    }
                }
                return list;
            }
        }
        public List<BlogPost> GetByCategory(string category)
        {
            List<BlogPost> list = new List<BlogPost>();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetBlogPostsByCategoryName", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CategoryName", category);
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        list.Add(Mapper.MapBlogPost(dr));
                    }
                }
                return list;
            }
        }

        public void CreateNewRelationBetweenBlogsAndTagsInTheBridgeTable(int blogId, int tagId)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("CreateBlogPostTag", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BlogId", blogId);
                cmd.Parameters.AddWithValue("@TagId", tagId);
                try
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
                catch (Exception e)
                {
                    string error = e.Message;
                }
            }
        }
    }
}