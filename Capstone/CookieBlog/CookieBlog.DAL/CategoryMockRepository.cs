﻿using CookieBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookieBlog.DAL
{
    public class CategoryMockRepository : ICategoryRepository
    {
        private List<Category> _list;

        public CategoryMockRepository()
        {

            if (_list == null)
            {
                _list = new List<Category>()
                {

                    new Category(){CategoryId = 1, CategoryName = "Chocolate Chip"},
                    new Category(){CategoryId = 2, CategoryName = "Snickerdoodle"}

                };
            }
        }
        public List<Category> GetAll()
        {
            return _list;
        }

        public Category GetById(int id)
        {
            return _list.FirstOrDefault(d => d.CategoryId == id);
        }

        public void Create(string category)
        {
            int nextID = 0;
            if (_list.Any())
            {
                nextID = _list.Max(d => d.CategoryId);
            }
            nextID++;
            var newCategory = new Category();
            newCategory.CategoryName = category;
            newCategory.CategoryId = nextID;
            _list.Add(newCategory);
        }

        public void Delete(int id)
        {
            for (int i = 0; i < _list.Count; i++)
            {
                if (id == _list.ToList()[i].CategoryId)
                {
                    _list.Remove(_list.ToList()[i]);
                }
            }

        }

        public void Update(Category category, int id)
        {
            for (int i = 0; i < _list.Count; i++)
            {
                if (id == _list[i].CategoryId)
                {
                    _list[i] = category;
                }
            }
        }

    }
}