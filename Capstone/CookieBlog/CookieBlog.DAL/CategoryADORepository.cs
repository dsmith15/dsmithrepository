﻿using CookieBlog.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
namespace CookieBlog.DAL
{
    public class CategoryADORepository : ICategoryRepository
    {
        private string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public void Create(string category)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("CreateCategories", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CategoryName", category);
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }
        public void Delete(int id)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("DeleteCategories", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CategoryId", id);
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }
        public List<Category> GetAll()
        {
            List<Category> list = new List<Category>();
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetAllCategories", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Category currentRow = new Category();
                        currentRow.CategoryId = int.Parse(dr["CategoryId"].ToString());
                        currentRow.CategoryName = dr["CategoryName"].ToString();

                        list.Add(currentRow);
                    }
                }
            }
            return list;
        }
        public Category GetById(int id)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetCategoriesById", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CategoryId", id);
                cn.Open();
                Category currentRow = new Category();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        currentRow.CategoryId = int.Parse(dr["CategoryId"].ToString());
                        currentRow.CategoryName = dr["CategoryName"].ToString();
                    }
                }
                return currentRow;
            }
        }
        public void Update(Category category, int id)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("UpdateCategories", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CategoryId", category.CategoryId);
                cmd.Parameters.AddWithValue("@CategoryName", category.CategoryName);
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }
        public Category GetByName(string name)
        {
            Category currentRow = new Category();

            using (var cn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetCategoriesByName", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CategoryName", name);
                cn.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        currentRow.CategoryId = int.Parse(dr["CategoryId"].ToString());
                        currentRow.CategoryName = dr["CategoryName"].ToString();
                    }
                }
            }
            return currentRow;
        }

    }
}
