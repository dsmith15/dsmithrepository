﻿using CookieBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace CookieBlog.DAL
{
    public class TagMockRepository : ITagRepository
    {
        private List<Tag> _list;
        public TagMockRepository()
        {
            if (_list == null)
            {
                _list = new List<Tag>();
                Tag tag1 = new Tag();
                Tag tag2 = new Tag();
                Tag tag3 = new Tag();
                Tag tag4 = new Tag();
                tag1.TagId = 1;
                tag1.TagName = "ChocolateChip";
                tag2.TagId = 2;
                tag2.TagName = "PeanutButter";
                tag3.TagId = 3;
                tag3.TagName = "Awesome";
                tag4.TagId = 4;
                tag4.TagName = "Cookie";
                _list.Add(tag1);
                _list.Add(tag2);
                _list.Add(tag3);
                _list.Add(tag4);
            }
        }
        public List<Tag> GetAll()
        {
            return _list;
        }
        public Tag GetById(int Id)
        {
            Tag tag = _list.FirstOrDefault(t => t.TagId == Id);
            return tag;
        }
        public void Create(Tag tag)
        {
            int nextId = 0;
            if (_list.Any())
            {
                nextId = _list.Max(t => t.TagId);
            }
            nextId++;
            tag.TagId = nextId;
            _list.Add(tag);
        }
        public void Update(Tag tag, int Id)
        {
            int x = _list.IndexOf(_list.FirstOrDefault(t => t.TagId == Id));
            _list[x].TagName = tag.TagName;
        }

        public Tag GetByName(string s)
        {
            throw new NotImplementedException();
        }

        public void Delete(int Id)
        {
            _list.Remove(_list.First(t => t.TagId == Id));
        }
    }
}


