﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CookieBlog.Models;
namespace CookieBlog.DAL
{
    public class BlogPostMockRepository : IBlogPostRepository
    {
        private List<BlogPost> _list;
        private Category _category1 = new Category() { CategoryId = 1, CategoryName = "Chocolate Chip" };
        private Category _category2 = new Category() { CategoryId = 2, CategoryName = "Snickerdoodle" };
        public BlogPostMockRepository()
        {
            if (_list == null)
            {
                _list = new List<BlogPost>()
                {
                    new BlogPost(){BlogId = 1, BlogTitle= "Why Chocolate Chip Cookies Are Going Out of Fashion.",
                        BlogImage = ("http://www.mommymusings.com/wp-content/uploads/2015/09/peanut-butter-spider-cookies-recipe.jpg"),
                        BlogContent = "Many, many words making my argument about why no one cares for chocolate chip cookies anymore.",
                        BlogStartDate = new DateTime(2017,10,3), BlogEndDate = null, Category = "Chocolate Chip", IsApproved = true, IsFeatured = true,
                        BlogTags = "ChocolateChip,Cookie"},
                    new BlogPost(){BlogId = 2, BlogTitle= "Why Sugar Cookies Are Going Out of Fashion.",
                        BlogImage = "http://media0.giphy.com/media/EKUvB9uFnm2Xe/giphy.gif",
                        BlogContent = "Many, many words making my argument about why no one cares for chocolate chip cookies anymore.",
                        BlogStartDate = new DateTime(2017,10,4), BlogEndDate = null, Category = "Snickerdoodle", IsApproved = true, IsFeatured = false,
                        BlogTags = "ChocolateChip,Awesome" }
                };
            }
        }
        public List<BlogPost> GetAll()
        {
            return _list;
        }
        public BlogPost GetById(int id)
        {
            return _list.Find(b => b.BlogId == id);
        }
        public int Create(BlogPost post)
        {
            int nextId = 0;
            if (_list.Any())
            {
                nextId = _list.Max(d => d.BlogId);
            }
            nextId++;
            post.BlogId = nextId;
            _list.Add(post);
            return post.BlogId;
        }
        public void Delete(int id)
        {
            for (int i = 0; i < _list.Count; i++)
            {
                if (id == _list.ToList()[i].BlogId)
                {
                    _list.Remove(_list.ToList()[i]);
                }
            }
        }
        public void Update(BlogPost post, int id)
        {
            for (int i = 0; i < _list.Count; i++)
            {
                if (id == _list[i].BlogId)
                {
                    _list[i] = post;
                }
            }
        }

        public void CreateNewRelationBetweenBlogsAndTagsInTheBridgeTable(int blogId, int tagId)
        {
            throw new NotImplementedException();
        }

        public List<BlogPost> GetMainBlogList()
        {
            throw new NotImplementedException();
        }

        public List<BlogPost> GetByTag(string tag)
        {
            throw new NotImplementedException();
        }
        public List<BlogPost> GetByCategory(string category)
        {
            CategoryMockRepository repo = new CategoryMockRepository();
            List<Category> categoryList = repo.GetAll();
            Category testCategory = new Category();
            foreach (var cat in categoryList)
            {
                if (cat.CategoryName == category)
                {
                    testCategory = cat;
                }
            }

            List<BlogPost> list = new List<BlogPost>();
            foreach (BlogPost post in _list)
            {
                if (post.Category == testCategory.CategoryName)
                {
                    list.Add(post);
                }
            }
            return list;
        }
        public List<BlogPost> GetByTitle(string title)
        {
            List<BlogPost> list = new List<BlogPost>();
            foreach (BlogPost post in _list)
            {
                if (post.BlogTitle == title)
                {
                    list.Add(post);
                }
            }
            return list;
        }
    }
}
