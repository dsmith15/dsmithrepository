﻿using System.Collections.Generic;
using CookieBlog.Models;

namespace CookieBlog.Models
{
    public interface IBlogPostRepository
    {
        int Create(BlogPost post);
        void Delete(int id);
        List<BlogPost> GetAll();
        List<BlogPost> GetByCategory(string category);
        BlogPost GetById(int id);
        List<BlogPost> GetByTag(string tag);
        List<BlogPost> GetByTitle(string title);
        void Update(BlogPost post, int id);
        void CreateNewRelationBetweenBlogsAndTagsInTheBridgeTable(int blogId, int tagId);
        List<BlogPost> GetMainBlogList();
    }
}