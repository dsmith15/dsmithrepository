﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookieBlog.Models.ViewModels
{
    public class CreateBlogPostVM
    {
        public BlogPost blogPost { get; set; }
        public Tag[] tagArray { get; set; }
        public List<Category> categoryList { get; set; }
    }
}



