﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookieBlog.Models.ViewModels
{
    public class BlogPostTagsVM
    {
        public List<BlogPost> BlogPostList { get; set; }
        public string TagNameList { get; set; }
    } 
}
