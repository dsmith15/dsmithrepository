﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CookieBlog.Models
{
    public class BlogPost : IValidatableObject
    {
        public int BlogId { get; set; }
        [Required(ErrorMessage = "Blog must have content")]
        [AllowHtml]
        public string BlogContent { get; set; }
        [Required(ErrorMessage = "You must enter a Title")]
        public string BlogTitle { get; set; }
        public bool IsApproved { get; set; }
        [Required(ErrorMessage = "You must enter a Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime BlogStartDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? BlogEndDate { get; set; }
        public string Category { get; set; }
        public bool IsFeatured { get; set; }
        public string BlogTags { get; set; }
        public string BlogImage { get; set; }
        public bool IsStatic { get; set; }
        public string Author { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (BlogEndDate != new DateTime() && BlogEndDate < BlogStartDate)
            {
                errors.Add(new ValidationResult("The Blog Cannot End Before it Starts", new[] { "BlogEndDate" }));
            }
            if (BlogEndDate != new DateTime() && BlogEndDate < DateTime.Now)
            {
                errors.Add(new ValidationResult("The Blog Cannot End Before Today", new[] { "BlogEndDate" }));
            }
            return errors;
        }
    }
}