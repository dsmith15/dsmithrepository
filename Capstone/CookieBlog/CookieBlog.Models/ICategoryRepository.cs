﻿using System.Collections.Generic;
using CookieBlog.Models;
namespace CookieBlog.Models
{
    public interface ICategoryRepository
    {
        void Create(string category);
        void Delete(int id);
        List<Category> GetAll();
        Category GetById(int id);
        void Update(Category category, int id);
    }
}