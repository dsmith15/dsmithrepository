﻿using System.Collections.Generic;
using CookieBlog.Models;

namespace CookieBlog.Models
{
    public interface ITagRepository
    {
        void Create(Tag tag);
        void Delete(int Id);
        List<Tag> GetAll();
        Tag GetById(int Id);
        void Update(Tag tag, int Id);
        Tag GetByName(string s);
    }
}