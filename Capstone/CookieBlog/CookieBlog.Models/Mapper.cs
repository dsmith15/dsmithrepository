﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookieBlog.Models
{
    public class Mapper
    {
        public static BlogPost MapBlogPost(SqlDataReader dr)
        {
            BlogPost currentRow = new BlogPost();
            currentRow.BlogId = int.Parse(dr["BlogId"].ToString());
            currentRow.BlogTitle = dr["BlogTitle"].ToString();
            currentRow.BlogContent = dr["BlogContent"].ToString();
            currentRow.IsApproved = (bool)dr["BlogIsApproved"];
            currentRow.BlogStartDate = DateTime.Parse(dr["BlogStartDate"].ToString());
            currentRow.BlogEndDate = null;
            if (dr["BlogEndDate"].ToString() != "")
            {
                currentRow.BlogEndDate = DateTime.Parse(dr["BlogEndDate"].ToString());
            }
            currentRow.Category = "";
            if (dr["CategoryName"].ToString() != "")
            {
                currentRow.Category = dr["CategoryName"].ToString();
            }
            currentRow.IsFeatured = (bool)dr["IsFeatured"];
            currentRow.BlogTags = "";
            if (dr["TagString"].ToString() != "")
            {
                currentRow.BlogTags = dr["TagString"].ToString();
            }
            currentRow.IsStatic = (bool)dr["IsStatic"];
            currentRow.Author = dr["Author"].ToString();
            currentRow.BlogImage = dr["ImageFileUrl"].ToString();
            return currentRow;
        }
    }
}
