﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Build.Framework;

namespace CookieBlog.Models
{
    public class Comment
    {
        public int CommentId { get; set; }
        [Required]
        public string CommentContent { get; set; }
        [Required]
        public DateTime CommentCreateDate { get; set; }
        public int BlogId { get; set; }
    }
}
