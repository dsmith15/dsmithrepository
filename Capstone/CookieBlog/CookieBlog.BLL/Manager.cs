﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using CookieBlog.DAL;
using CookieBlog.Models;
using CookieBlog.Models.ViewModels;

namespace CookieBlog.BLL
{
    public class Manager
    {
        private ITagRepository _tagRepository;
        private ICategoryRepository _categoryRepository;
        private IBlogPostRepository _blogPostRepository;

        public Manager(ITagRepository tagRepository, ICategoryRepository categoryRepository,
            IBlogPostRepository blogPostRepository)
        {
            _tagRepository = tagRepository;
            _categoryRepository = categoryRepository;
            _blogPostRepository = blogPostRepository;
        }

        public BlogPost GetAnyBlogPost(int id)
        {
            BlogPost blog = _blogPostRepository.GetById(id);

            return blog;
        }

        public void Delete(int id)
        {
            _blogPostRepository.Delete(id);
        }

        public List<BlogPost> GetAllBlogPost()
        {
            List<BlogPost> list = _blogPostRepository.GetAll();

            return list;
        }

        public BlogPostTagsVM GetAllBlogPostTagsVm()
        {
            BlogPostTagsVM blogPostTagsVm = new BlogPostTagsVM();

            blogPostTagsVm.BlogPostList = GetAllBlogPost();

            List<Tag> list = _tagRepository.GetAll();

            foreach (var l in list)
            {
                blogPostTagsVm.TagNameList += l.TagName + ",";
            }

            blogPostTagsVm.TagNameList.TrimEnd(',');

            return blogPostTagsVm;
        }

        public List<Tag> GetAllTags()
        {
            List<Tag> list = _tagRepository.GetAll();

            return list;
        }

        public List<Category> GetAllCategories()
        {
            List<Category> list = _categoryRepository.GetAll();
            return list;
        }

        public BlogPost CreateNewBlogPost(BlogPost blog)
        {
            if (!string.IsNullOrEmpty(blog.Category))
            {
                List<Category> testCategories = _categoryRepository.GetAll();
                bool doesExist = false;
                foreach (Category cat in testCategories)
                {
                    if (blog.Category == cat.CategoryName)
                    {
                        doesExist = true;
                        break;
                    }
                    else
                    {
                        doesExist = false;
                    }
                }
                if (doesExist == false)
                {
                    _categoryRepository.Create(blog.Category);
                }
            }

            if (!string.IsNullOrEmpty(blog.BlogTags))
            {
                List<string> tagList = blog.BlogTags.Split(',').ToList();
                List<Tag> allTags = _tagRepository.GetAll();
                List<string> newTags = new List<string>();

                foreach (var item in tagList)
                {
                    string tagItem = item;
                    bool addTag = true;
                    foreach (var thing in allTags)
                    {
                        if (tagItem == thing.TagName)
                        {
                            addTag = false;
                        }
                    }
                    if (addTag)
                    {
                        newTags.Add(tagItem);
                    }
                }
                int tagCount = allTags.Count();
                if (newTags.Count != 0)
                {
                    foreach (var item in newTags)
                    {
                        Tag dataTag = new Tag()
                        {
                            TagName = item,
                            TagId = tagCount + 1
                        };
                        tagCount++;
                        _tagRepository.Create(dataTag);
                    }
                }
            }

            blog.BlogId = _blogPostRepository.Create(blog);

            return blog;
        }
        public BlogPost UpdateBlogPost(BlogPost blog)
        {
            if (!string.IsNullOrEmpty(blog.Category))
            {
                List<Category> testCategories = _categoryRepository.GetAll();
                bool doesExist = false;
                foreach (Category cat in testCategories)
                {
                    if (blog.Category == cat.CategoryName)
                    {
                        doesExist = true;
                        break;
                    }
                    else
                    {
                        doesExist = false;
                    }
                }
                if (doesExist == false)
                {
                    _categoryRepository.Create(blog.Category);
                }
            }

            if (!string.IsNullOrEmpty(blog.BlogTags))
            {
                List<string> tagList = blog.BlogTags.Split(',').ToList();
                List<Tag> allTags = _tagRepository.GetAll();
                List<string> newTags = new List<string>();

                foreach (var item in tagList)
                {
                    string tagItem = item;
                    bool addTag = true;
                    foreach (var thing in allTags)
                    {
                        if (tagItem == thing.TagName)
                        {
                            addTag = false;
                        }
                    }
                    if (addTag)
                    {
                        newTags.Add(tagItem);
                    }
                }
                int tagCount = allTags.Count();
                if (newTags.Count != 0)
                {
                    foreach (var item in newTags)
                    {
                        Tag dataTag = new Tag()
                        {
                            TagName = item,
                            TagId = tagCount + 1
                        };
                        tagCount++;
                        _tagRepository.Create(dataTag);
                    }
                }
            }

            _blogPostRepository.Update(blog, blog.BlogId);
            return blog;
        }
    }
}
