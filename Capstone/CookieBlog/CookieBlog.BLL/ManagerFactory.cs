﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CookieBlog.DAL;

namespace CookieBlog.BLL
{
    public class ManagerFactory
    {
        public static Manager CreateManager()
        {
            string mode = ConfigurationManager.AppSettings["Mode"].ToString();

            switch (mode)
            {
                case "SampleData":
                    return new Manager(new TagMockRepository(), new CategoryMockRepository(), new BlogPostMockRepository());
                case "ADO":
                    return new Manager(new TagADORepository(), new CategoryADORepository(), new BlogPostADORepository());
                default:
                    throw new Exception("Mode value in web config is not valid.");
            }
        }

    }
}
