﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CookieBlog.BLL;
using CookieBlog.DAL;
using CookieBlog.Models;
using CookieBlog.Models.ViewModels;
using NUnit.Framework;

namespace CookieBlog.Tests
{
    [TestFixture]
    public class Tests
    {
        private string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        [Test]
        public void TagMockGetAll()
        {
            TagMockRepository repo = new TagMockRepository();
            List<Tag> list = repo.GetAll();

            Assert.AreEqual(4, list.Count);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void TagMockGetById(int id)
        {
            TagMockRepository repo = new TagMockRepository();

            Tag newTag = repo.GetById(id);

            Assert.AreEqual(id, newTag.TagId);
        }

        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5)]
        public void TagMockCreate(int id)
        {
            TagMockRepository repo = new TagMockRepository();

            Tag actualTag = new Tag()
            {
                TagId = id,
                TagName = "Snickerdoodle"
            };
            repo.Create(actualTag);
            Tag testTag = repo.GetById(id);
            Assert.AreEqual(id, testTag.TagId);
        }

        [Test]
        public void TagMockRepoDelete()
        {
            TagMockRepository repo = new TagMockRepository();

            Tag actualTag = new Tag()
            {
                TagId = 3,
                TagName = "Snickerdoodle"
            };
            repo.Create(actualTag);
            Tag testTag = repo.GetById(3);
            repo.Delete(3);
            List<Tag> list = repo.GetAll();
            Assert.AreEqual(3, testTag.TagId);
            Assert.AreEqual(4, list.Count);
        }

        [Test]
        public void TagMockRepoUpdate()
        {
            TagMockRepository repo = new TagMockRepository();

            Tag actualTag = new Tag()
            {
                TagId = 3,
                TagName = "Snickerdoodle"
            };
            repo.Create(actualTag);
            Tag testTag = repo.GetById(3);

            testTag.TagName = "R";

            repo.Update(testTag, 3);

            Tag newTestTag = repo.GetById(3);
            Assert.AreEqual("R", newTestTag.TagName);
        }

        [Test]
        public void CategoryMockGetAll()
        {
            CategoryMockRepository repo = new CategoryMockRepository();
            List<Category> list = repo.GetAll();

            Assert.AreEqual(2, list.Count);
        }

        [TestCase(1)]
        public void CategoryMockGetById(int id)
        {
            CategoryMockRepository repo = new CategoryMockRepository();

            Category newCategory = repo.GetById(id);

            Assert.AreEqual(1, newCategory.CategoryId);
        }

        [Test]
        public void CategoryMockCreate()
        {
            CategoryMockRepository repo = new CategoryMockRepository();

            var actualCategory = "Snickerdoodle";

            repo.Create(actualCategory);
            Category testCategory = repo.GetById(3);
            Assert.AreEqual(actualCategory, testCategory.CategoryName);
        }

        [Test]
        public void CategoryMockRepoDelete()
        {
            CategoryMockRepository repo = new CategoryMockRepository();

            var actualCategory = "Snickerdoodle";
            repo.Create(actualCategory);
            Category testCategory = repo.GetById(3);
            repo.Delete(3);
            List<Category> list = repo.GetAll();
            Assert.AreEqual(actualCategory, testCategory.CategoryName);
            Assert.AreEqual(2, list.Count);
        }

        [Test]
        public void CategoryMockRepoUpdate()
        {
            CategoryMockRepository repo = new CategoryMockRepository();

            var actualCategory = "Snickerdoodle";
            repo.Create(actualCategory);
            Category testCategory = repo.GetById(3);

            testCategory.CategoryName = "R";

            repo.Update(testCategory, 3);

            Category newTestCategory = repo.GetById(3);
            Assert.AreEqual("R", newTestCategory.CategoryName);
        }

        [Test]
        public void BlogPostMockGetAll()
        {
            BlogPostMockRepository repo = new BlogPostMockRepository();
            List<BlogPost> list = repo.GetAll();

            Assert.AreEqual(2, list.Count);
        }

        [TestCase(1)]
        public void BlogPostMockGetById(int id)
        {
            BlogPostMockRepository repo = new BlogPostMockRepository();

            BlogPost newBlogPost = repo.GetById(id);

            Assert.AreEqual(1, newBlogPost.BlogId);
        }

        [Test]
        public void BlogPostMockCreate()
        {
            BlogPostMockRepository repo = new BlogPostMockRepository();

            BlogPost actualBlogPost = new BlogPost()
            {
                BlogTitle = "Why Plain Cookies Are Going Out of Fashion.",
                BlogContent = "Many, many words making my argument about why no one cares for plain cookies anymore.",
                BlogStartDate = new DateTime(10 / 4 / 2017),
                BlogEndDate = null
            };
            repo.Create(actualBlogPost);
            BlogPost testBlogPost = repo.GetById(3);
            Assert.AreEqual(actualBlogPost.BlogId, testBlogPost.BlogId);
        }

        [Test]
        public void BlogPostMockRepoDelete()
        {
            BlogPostMockRepository repo = new BlogPostMockRepository();

            BlogPost actualBlogPost = new BlogPost()
            {
                BlogTitle = "Why Plain Cookies Are Going Out of Fashion.",
                BlogContent = "Many, many words making my argument about why no one cares for plain cookies anymore.",
                BlogStartDate = new DateTime(10 / 4 / 2017),
                BlogEndDate = null
            };
            repo.Create(actualBlogPost);
            BlogPost testBlogPost = repo.GetById(3);
            repo.Delete(3);
            List<BlogPost> list = repo.GetAll();
            Assert.AreEqual(actualBlogPost.BlogId, testBlogPost.BlogId);
            Assert.AreEqual(2, list.Count);
        }

        [Test]
        public void BlogPostMockRepoUpdate()
        {
            BlogPostMockRepository repo = new BlogPostMockRepository();

            BlogPost actualBlogPost = new BlogPost()
            {
                BlogTitle = "Why Plain Cookies Are Going Out of Fashion.",
                BlogContent = "Many, many words making my argument about why no one cares for plain cookies anymore.",
                BlogStartDate = new DateTime(10 / 4 / 2017),
                BlogEndDate = null
            };
            repo.Create(actualBlogPost);
            BlogPost testBlogPost = repo.GetById(3);

            testBlogPost.BlogContent = "R";

            repo.Update(testBlogPost, 3);

            BlogPost newTestBlogPost = repo.GetById(3);
            Assert.AreEqual("R", newTestBlogPost.BlogContent);
        }

        [Test]
        public void BlogPostMockRepoGetByCategory()
        {
            BlogPostMockRepository repo = new BlogPostMockRepository();

            string category = "Snickerdoodle";

            List<BlogPost> list = repo.GetByCategory(category);
            Assert.AreEqual(1, list.Count);
        }

        [Test]
        public void BlogPostMockRepoGetByTitle()
        {
            BlogPostMockRepository repo = new BlogPostMockRepository();

            string title = "Why Chocolate Chip Cookies Are Going Out of Fashion.";

            List<BlogPost> list = repo.GetByTitle(title);
            Assert.AreEqual(1, list.Count);
        }

        [Test]
        public void CategoryADOGetAll()
        {
            CategoryADORepository repo = new CategoryADORepository();
            List<Category> list = repo.GetAll();

            List<Category> newList = repo.GetAll();

            Assert.AreEqual(newList.Count, list.Count);
        }

        [TestCase(1)]
        [TestCase(9)]
        public void CategoryADOGetById(int id)
        {
            CategoryADORepository repo = new CategoryADORepository();

            Category newCategory = repo.GetById(id);

            Assert.AreEqual(id, newCategory.CategoryId);
        }

        [TestCase("Updates")]
        [TestCase("Snickerdoodle")]
        public void CategoryADOGetByName(string name)
        {
            CategoryADORepository repo = new CategoryADORepository();

            Category newCategory = repo.GetByName(name);

            Assert.AreEqual(name, newCategory.CategoryName);
        }

        [Test]
        public void CategoryADOCreate()
        {
            CategoryADORepository repo = new CategoryADORepository();

            var actualCategory = "Snickerdoodle";
            repo.Create(actualCategory);
            int newId = repo.GetAll().Max(c => c.CategoryId);
            Category testCategory = repo.GetById(newId);
            Assert.AreEqual(actualCategory, testCategory.CategoryName);
        }

        [Test]
        public void CategoryADORepoUpdate()
        {
            CategoryADORepository repo = new CategoryADORepository();

            Category testCategory = repo.GetById(6);

            testCategory.CategoryName = "Oatmeal Raisin";

            repo.Update(testCategory, 6);

            Category newTestCategory = repo.GetById(6);
            Assert.AreEqual("Oatmeal Raisin", newTestCategory.CategoryName);
        }

        [Test]
        public void TagADOGetAll()
        {
            TagADORepository repo = new TagADORepository();
            List<Tag> list = repo.GetAll();

            List<Tag> expectedList = repo.GetAll();

            Assert.AreEqual(expectedList.Count, list.Count);
        }

        [TestCase(1)]
        [TestCase(4)]
        public void TagADOGetById(int id)
        {
            TagADORepository repo = new TagADORepository();

            Tag newTag = repo.GetById(id);

            Assert.AreEqual(id, newTag.TagId);
        }

        [Test]
        public void TagADOCreate()
        {
            TagADORepository repo = new TagADORepository();

            Tag actualTag = new Tag()
            {
                TagId = 10,
                TagName = "Yummy"
            };
            repo.Create(actualTag);
            Tag testTag = repo.GetById(10);
            Assert.AreEqual(actualTag.TagId, testTag.TagId);
        }

        [Test]
        public void TagADORepoUpdate()
        {
            TagADORepository repo = new TagADORepository();

            Tag testTag = repo.GetById(6);

            testTag.TagName = "Hipster";

            repo.Update(testTag, 6);

            Tag newTestTag = repo.GetById(6);
            Assert.AreEqual("Hipster", newTestTag.TagName);
        }

        [Test]
        public void BlogPostADOGetAll()
        {
            BlogPostADORepository repo = new BlogPostADORepository();
            List<BlogPost> list = repo.GetAll();

            List<BlogPost> newList = repo.GetAll();

            Assert.AreEqual(newList.Count, list.Count);
        }

        [TestCase(7)]
        public void BlogPostADOGetById(int id)
        {
            BlogPostADORepository repo = new BlogPostADORepository();

            BlogPost newBlogPost = repo.GetById(id);

            Assert.AreEqual(id, newBlogPost.BlogId);
        }

        [Test]
        public void BlogPostADOCreate()
        {
            BlogPostADORepository repo = new BlogPostADORepository();

            BlogPost actualBlogPost = new BlogPost()
            {
                BlogTitle = "Why Plain Cookies Are Going Out of Fashion.",
                BlogContent = "Many, many words making my argument about why no one cares for plain cookies anymore.",
                BlogStartDate = new DateTime(2017, 10, 5),
                BlogEndDate = null,
                Category = "Snickerdoodle",
                IsApproved = true,
                IsFeatured = false,
                BlogImage = " "
            };
            repo.Create(actualBlogPost);
            BlogPost testBlogPost = repo.GetById(3);
            Assert.AreEqual(3, testBlogPost.BlogId);
        }

        [Test]
        public void BlogPostADORepoUpdate()
        {
            BlogPostADORepository repo = new BlogPostADORepository();

            BlogPost testBlogPost = repo.GetById(7);

            testBlogPost.BlogContent = "Much many words in an overwritten, bombastic tone.";

            repo.Update(testBlogPost, 7);

            BlogPost newTestBlogPost = repo.GetById(7);
            Assert.AreEqual("Much many words in an overwritten, bombastic tone.", newTestBlogPost.BlogContent);
        }

        [Test]
        public void BlogPostADORepoGetByCategory()
        {
            BlogPostADORepository repo = new BlogPostADORepository();

            string category = "Updates";

            List<BlogPost> list = repo.GetByCategory(category);
            Assert.AreEqual(12, list.Count);
        }

        [Test]
        public void BlogPostADORepoGetByTitle()
        {
            BlogPostADORepository repo = new BlogPostADORepository();

            string title = "Grand Opening";

            List<BlogPost> list = repo.GetByTitle(title);
            Assert.AreEqual(1, list.Count);
        }


        [Test]
        public void GetAnyBlogPostVMTest()
        {
            Manager manager = ManagerFactory.CreateManager();

            BlogPost actualBlogPost = manager.GetAnyBlogPost(7);

            BlogPost expectBlogPost = new BlogPost();

            expectBlogPost.BlogId = 7;
            expectBlogPost.BlogTitle = "Why Chocolate Chip Cookies Are Going Out of Fashion.";
            expectBlogPost.Category = "Chocolate Chip";

            Assert.AreEqual(expectBlogPost.BlogId, actualBlogPost.BlogId);
        }

        [Test]
        public void GetApprovedBlogPostVMListTest()
        {
            Manager manager = ManagerFactory.CreateManager();

            List<BlogPost> actualBlogPost = manager.GetAllBlogPost();

            List<BlogPost> expectBlogPost = new List<BlogPost>();

            var expectedBlogPostOne = manager.GetAnyBlogPost(2);
            var expectedBlogPostTwo = manager.GetAnyBlogPost(3);

            expectBlogPost.Add(expectedBlogPostOne);
            expectBlogPost.Add(expectedBlogPostTwo);

            Assert.AreEqual(expectBlogPost[0].BlogId, actualBlogPost[0].BlogId);
            Assert.AreEqual(expectBlogPost[1].BlogTitle, actualBlogPost[1].BlogTitle);
        }

        [Test]
        public void GetApprovedBlogPostTagVMTest()
        {
            Manager manager = ManagerFactory.CreateManager();

            BlogPostTagsVM actualBlogPostTagsVm = manager.GetAllBlogPostTagsVm();

            BlogPostTagsVM expectedBlogPostTagsVm = new BlogPostTagsVM();

            List<BlogPost> expectBlogPostVm = new List<BlogPost>();

            var expectedBlogPostOne = manager.GetAnyBlogPost(2);
            var expectedBlogPostTwo = manager.GetAnyBlogPost(3);

            expectBlogPostVm.Add(expectedBlogPostOne);
            expectBlogPostVm.Add(expectedBlogPostTwo);

            expectedBlogPostTagsVm.BlogPostList = expectBlogPostVm;

            Assert.AreEqual(expectedBlogPostTagsVm.BlogPostList[0].BlogId, actualBlogPostTagsVm.BlogPostList[0].BlogId);
        }


    }
}
