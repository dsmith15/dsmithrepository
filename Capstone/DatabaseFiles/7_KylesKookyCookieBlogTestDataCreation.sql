USE KylesKookyCookieBlog
go
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES 
    Where ROUTINE_NAME = 'CookieBlogDbReset')
        DROP PROCEDURE CookieBlogDbReset
GO
        
CREATE PROCEDURE CookieBlogDbReset AS
BEGIN
    DELETE FROM Comments;
    DELETE FROM BlogPosts;
    DELETE FROM Tags;
    DELETE FROM Categories;
    
set identity_insert Categories on;
    INSERT INTO Categories (CategoryId, CategoryName)
    Values (0, 'Uncategorized'),
    (1, 'Updates'),
    (2, 'Low Cal'),
    (3, 'Gluten-Free')
    
    set identity_insert Categories off;
    
        set identity_insert Tags on;
    INSERT INTO Tags (TagId, TagName)
    Values (1, 'Chocolate Chip'),
    (2,'Peanut Butter'),
    (3, 'Snickerdoodle'),
    (4, 'Christmas'),
    (5, 'Halloween'),
    (6, 'Homemade'),
    (7, 'Organic'),
    (8, 'New Flavor'),
    (9, 'Random')
    
    set identity_insert Tags off;
    set identity_insert BlogPosts on;
    INSERT INTO BlogPosts (BlogId, BlogTitle, BlogContent, BlogIsApproved, BlogCreateDate, BlogStartDate, BlogEndDate, CategoryId, IsFeatured, UserId, ImageFileUrl, TagString, IsStatic, Author)
    Values (1, 'First', 'First', 1, Convert(datetime,'Oct 23 5:50:00 2017'), CONVERT(datetime,'Oct 29 12:18:52 2017'), null, 0, 0, null, 'https://m.popkey.co/f6d4e2/rwqz7.gif', 'Random,New Flavor', 1, 'adminkyle@CookieBlog.com'),
    (2, 'Grand Opening', 'Come to our Grand Opening!', 0, Convert(datetime,'Oct 23 6:00:00 2017'), Convert(datetime,'Nov 1 6:00:00 2017'), Convert(datetime,'Nov 28 23:59:59 2017'), 1, 1, null, 'https://media.tenor.com/images/fdefee388ad822a4c396792728cacb55/tenor.gif', 'Random,Organic,New Flavor', 1, 'adminkyle@CookieBlog.com');
    
    set identity_insert BlogPosts off;
    
   END
