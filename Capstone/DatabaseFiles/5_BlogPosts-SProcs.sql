USE KylesKookyCookieBlog
IF EXISTS(Select * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'GetAllBlogPosts')
        DROP PROCEDURE GetAllBlogPosts
GO
CREATE PROCEDURE GetAllBlogPosts AS
BEGIN
    SELECT        b.BlogId, b.BlogTitle, b.BlogContent, b.BlogIsApproved, b.BlogCreateDate, b.BlogStartDate, b.BlogEndDate, b.IsFeatured, b.ImageFileUrl, b.TagString, b.IsStatic, b.Author, Categories.CategoryName
FROM            BlogPosts AS b INNER JOIN
                         Categories ON b.CategoryId = Categories.CategoryId
END
GO
IF EXISTS(Select * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'GetMainBlogList')
        DROP PROCEDURE GetMainBlogList
GO
CREATE PROCEDURE GetMainBlogList AS
BEGIN
    SELECT        b.BlogId, b.BlogTitle, b.BlogContent, b.BlogIsApproved, b.BlogCreateDate, b.BlogStartDate, b.BlogEndDate, b.IsFeatured, b.ImageFileUrl, b.TagString, b.IsStatic, b.Author, Categories.CategoryName
FROM            BlogPosts AS b INNER JOIN
                         Categories ON b.CategoryId = Categories.CategoryId
WHERE        (BlogIsApproved = 1)and (BlogStartDate <= CONVERT(date, SysDateTime())) and (BlogEndDate >= CONVERT(date, SysDateTime()))
OR
((BlogIsApproved = 1)and (BlogStartDate <= CONVERT(date, SysDateTime()) and (BlogEndDate is Null)))
END
GO
IF EXISTS(Select * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'GetBlogPostsById')
        DROP PROCEDURE GetBlogPostsById
GO
CREATE PROCEDURE GetBlogPostsById (@BlogId int) AS
BEGIN
    SELECT        b.BlogId, b.BlogTitle, b.BlogContent, b.BlogIsApproved, b.BlogCreateDate, b.BlogStartDate, b.BlogEndDate, b.IsFeatured, b.ImageFileUrl, b.TagString, b.IsStatic, b.Author, Categories.CategoryName
FROM            BlogPosts AS b INNER JOIN
                         Categories ON b.CategoryId = Categories.CategoryId
WHERE        (b.BlogId = @BlogId)
END
GO
IF EXISTS(Select * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'GetBlogPostsByTitle')
        DROP PROCEDURE GetBlogPostsByTitle
GO
CREATE PROCEDURE GetBlogPostsByTitle (@BlogTitle nvarchar(140)) AS
BEGIN
    SELECT        b.BlogId, b.BlogTitle, b.BlogContent, b.BlogIsApproved, b.BlogCreateDate, b.BlogStartDate, b.BlogEndDate, b.IsFeatured, b.ImageFileUrl, b.TagString, b.IsStatic, b.Author, Categories.CategoryName
FROM            BlogPosts AS b INNER JOIN
                         Categories ON b.CategoryId = Categories.CategoryId
WHERE        (BlogTitle LIKE @BlogTitle)
END
GO
IF EXISTS(Select * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'GetBlogPostsByCategoryId')
        DROP PROCEDURE GetBlogPostsByCategoryId
GO
CREATE PROCEDURE GetBlogPostsByCategoryId (@CategoryId int) AS
BEGIN
    SELECT        b.BlogId, b.BlogTitle, b.BlogContent, b.BlogIsApproved, b.BlogCreateDate, b.BlogStartDate, b.BlogEndDate, b.IsFeatured, b.ImageFileUrl, b.TagString, b.IsStatic, b.Author, Categories.CategoryName
FROM            BlogPosts AS b INNER JOIN
                         Categories ON b.CategoryId = Categories.CategoryId
END
GO
IF EXISTS(Select * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'GetBlogPostsByCategoryName')
        DROP PROCEDURE GetBlogPostsByCategoryName
GO
CREATE PROCEDURE GetBlogPostsByCategoryName (@CategoryName nvarchar(30)) AS
BEGIN
    SELECT        b.BlogId, b.BlogTitle, b.BlogContent, b.BlogIsApproved, b.BlogCreateDate, b.BlogStartDate, b.BlogEndDate, b.IsFeatured, b.ImageFileUrl, b.TagString, b.IsStatic, b.Author, Categories.CategoryName
FROM            BlogPosts AS b INNER JOIN
                         Categories ON b.CategoryId = Categories.CategoryId
WHERE        Categories.CategoryName = @CategoryName
END
GO
IF EXISTS(Select * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'GetBlogPostsByTag')
        DROP PROCEDURE GetBlogPostsByTag
GO
CREATE PROCEDURE GetBlogPostsByTag (@TagName nvarchar(140)) AS
BEGIN
    SELECT        b.BlogId, b.BlogTitle, b.BlogContent, b.BlogIsApproved, b.BlogCreateDate, b.BlogStartDate, b.BlogEndDate, b.IsFeatured, b.ImageFileUrl, b.TagString, b.IsStatic, b.Author, Categories.CategoryName
FROM            BlogPosts AS b INNER JOIN
                         Categories ON b.CategoryId = Categories.CategoryId
WHERE        (b.TagString Like Concat('%', @TagName ,'%'))
END
GO
IF EXISTS(Select * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'GetBlogPostsByStartDate')
        DROP PROCEDURE GetBlogPostsByStartDate
GO
CREATE PROCEDURE GetBlogPostsByStartDate (@BlogStartDate datetime2) AS
BEGIN
    SELECT        b.BlogId, b.BlogTitle, b.BlogContent, b.BlogIsApproved, b.BlogCreateDate, b.BlogStartDate, b.BlogEndDate, b.IsFeatured, b.ImageFileUrl, b.TagString, b.IsStatic, b.Author, Categories.CategoryName
FROM            BlogPosts AS b INNER JOIN
                         Categories ON b.CategoryId = Categories.CategoryId
WHERE        (BlogStartDate = @BlogStartDate)
END
GO
IF EXISTS(Select * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'GetBlogPostsByCreateDate')
        DROP PROCEDURE GetBlogPostsByCreateDate
GO
CREATE PROCEDURE GetBlogPostsByCreateDate (@BlogCreateDate datetime2) AS
BEGIN
    SELECT        b.BlogId, b.BlogTitle, b.BlogContent, b.BlogIsApproved, b.BlogCreateDate, b.BlogStartDate, b.BlogEndDate, b.IsFeatured, b.ImageFileUrl, b.TagString, b.IsStatic, b.Author, Categories.CategoryName
FROM            BlogPosts AS b INNER JOIN
                         Categories ON b.CategoryId = Categories.CategoryId
WHERE        (BlogCreateDate = @BlogCreateDate)
END
GO
IF EXISTS(Select * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'GetBlogPostsByEndDate')
        DROP PROCEDURE GetBlogPostsByEndDate
GO
CREATE PROCEDURE GetBlogPostsByEndDate (@BlogEndDate datetime2) AS
BEGIN
    SELECT        b.BlogId, b.BlogTitle, b.BlogContent, b.BlogIsApproved, b.BlogCreateDate, b.BlogStartDate, b.BlogEndDate, b.IsFeatured, b.ImageFileUrl, b.TagString, b.IsStatic, b.Author, Categories.CategoryName
FROM            BlogPosts AS b INNER JOIN
                         Categories ON b.CategoryId = Categories.CategoryId
WHERE        (BlogEndDate = @BlogEndDate)
END
GO
IF EXISTS(Select * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'GetBlogPostsByIsFeatured')
        DROP PROCEDURE GetBlogPostsByIsFeatured
GO
CREATE PROCEDURE GetBlogPostsByIsFeatured (@IsFeatured bit) AS
BEGIN
    SELECT        b.BlogId, b.BlogTitle, b.BlogContent, b.BlogIsApproved, b.BlogCreateDate, b.BlogStartDate, b.BlogEndDate, b.IsFeatured, b.ImageFileUrl, b.TagString, b.IsStatic, b.Author, Categories.CategoryName
FROM            BlogPosts AS b INNER JOIN
                         Categories ON b.CategoryId = Categories.CategoryId
WHERE        (IsFeatured = 1)
END
GO
IF EXISTS(Select * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'GetBlogPostsByIsApproved')
        DROP PROCEDURE GetBlogPostsByIsApproved
GO
CREATE PROCEDURE GetBlogPostsByIsApproved(@BlogIsApproved bit) AS
BEGIN
    SELECT        b.BlogId, b.BlogTitle, b.BlogContent, b.BlogIsApproved, b.BlogCreateDate, b.BlogStartDate, b.BlogEndDate, b.IsFeatured, b.ImageFileUrl, b.TagString, b.IsStatic, b.Author, Categories.CategoryName
FROM            BlogPosts AS b INNER JOIN
                         Categories ON b.CategoryId = Categories.CategoryId
WHERE        (BlogIsApproved = 1)
END
GO
IF EXISTS(Select * FROM INFORMATION_SCHEMA.ROUTINES
    WHERE ROUTINE_NAME = 'GetBlogPostsByIsNotApproved')
        DROP PROCEDURE GetBlogPostsByIsNotApproved
GO
CREATE PROCEDURE GetBlogPostsByIsNotApproved(@BlogIsApproved bit) AS
BEGIN
    SELECT        b.BlogId, b.BlogTitle, b.BlogContent, b.BlogIsApproved, b.BlogCreateDate, b.BlogStartDate, b.BlogEndDate, b.IsFeatured, b.ImageFileUrl, b.TagString, b.IsStatic, b.Author, Categories.CategoryName
FROM            BlogPosts AS b INNER JOIN
                         Categories ON b.CategoryId = Categories.CategoryId
WHERE        (BlogIsApproved = 0)
END
GO
GO
If exists(Select * from INFORMATION_SCHEMA.ROUTINES
    Where Routine_Name = 'CreateBlogPost')
        DROP Procedure CreateBlogPost
GO
Create Procedure CreateBlogPost (
    @BlogTitle nvarchar(140),
    @BlogContent nvarchar(max),
    @BlogIsApproved bit,
    @BlogStartDate datetime2,
    @BlogEndDate datetime2,
    @CategoryName nvarchar(30) ,
    @IsFeatured bit,
    @ImageFileUrl nvarchar(140),
    @TagString nvarchar(max),
    @IsStatic bit,
	@Author nvarchar(140)
    ) As
    Begin
    Insert Into BlogPosts (BlogTitle, BlogContent, BlogIsApproved, BlogStartDate, BlogEndDate, CategoryId, IsFeatured, ImageFileUrl, TagString, IsStatic, Author)
    
    Values (@BlogTitle, @BlogContent, @BlogIsApproved, @BlogStartDate , @BlogEndDate, (Select CategoryId
    From Categories 
    Where CategoryName = @CategoryName), @IsFeatured, @ImageFileUrl, @TagString, @IsStatic, @Author)
    SELECT @@IDENTITY AS BlogId;
    ;
END
GO
If exists(Select * from INFORMATION_SCHEMA.ROUTINES
    Where Routine_Name = 'UpdateBlogPost')
        DROP Procedure UpdateBlogPost
GO
Create Procedure UpdateBlogPost (
    @BlogId int,
    @BlogTitle nvarchar(140),
    @BlogContent nvarchar(max),
    @BlogIsApproved bit,
    @BlogStartDate datetime2,
    @BlogEndDate datetime2,
    @CategoryName nvarchar(30),
    @IsFeatured bit,
    @ImageFileUrl nvarchar(140),
    @TagString nvarchar(max),
    @IsStatic bit,
	@Author nvarchar(140)
    ) As
    Begin
    Update BlogPosts SET 
    BlogTitle = @BlogTitle, 
    BlogContent = @BlogContent, 
    BlogIsApproved = @BlogIsApproved, 
    BlogStartDate = @BlogStartDate, 
    BlogEndDate = @BlogEndDate, 
    CategoryId = (Select CategoryId
    From Categories 
    Where CategoryName = @CategoryName),
    IsFeatured = @IsFeatured,
    ImageFileUrl = @ImageFileUrl,
    TagString = @TagString,
    IsStatic = @IsStatic,
	Author = @Author
Where BlogId = @BlogId;
END
GO
If exists(Select * from INFORMATION_SCHEMA.ROUTINES
    Where Routine_Name = 'DeleteBlogPost')
        DROP Procedure DeleteBlogPost
GO
Create Procedure DeleteBlogPost (
    @BlogId int
    ) As
    Begin
        Begin transaction
    Delete from BlogPosts Where BlogId = @BlogId;
    Commit transaction
End
GO
