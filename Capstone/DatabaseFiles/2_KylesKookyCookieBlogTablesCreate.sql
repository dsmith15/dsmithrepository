USE KylesKookyCookieBlog
go
IF EXISTS(Select * FROM sys.tables WHERE name ='Comments')
    DROP TABLE Comments
GO
IF EXISTS(Select * FROM sys.tables WHERE name ='BlogPosts')
    DROP TABLE BlogPosts
GO
IF EXISTS(Select * FROM sys.tables WHERE name ='Tags')
    DROP TABLE Tags
GO
IF EXISTS(Select * FROM sys.tables WHERE name ='Categories')
    DROP TABLE Categories
GO
CREATE TABLE Categories(
    CategoryId int identity(0,1) not null primary key,
    CategoryName nvarchar(30) not null
)
GO
CREATE TABLE Tags(
    TagId int identity(1,1) not null primary key,
    TagName nvarchar(140) not null
)
GO
CREATE TABLE BlogPosts(
    BlogId int identity(1,1) not null primary key,
    BlogTitle nvarchar(140) not null,
    BlogContent nvarchar(max) not null,
    BlogIsApproved bit not null,
    BlogCreateDate datetime2 not null
        Constraint DF_BlogPosts_BlogCreateDate Default (SYSDATETIME()), 
    BlogStartDate datetime2 not null,
    BlogEndDate datetime2 null,
    CategoryId int foreign key references Categories(CategoryId) not null,
    IsFeatured bit not null,
    UserId nvarchar(128) null,
    ImageFileUrl nvarchar(140) null,
    TagString nvarchar(max) null,
    IsStatic bit not null,
	Author nvarchar(140) not null
)
GO
CREATE TABLE Comments(
    CommentId int identity(1,1) not null primary key,
    CommentContent nvarchar(140) not null, 
    CommentCreateDate datetime2 not null, 
    BlogId int foreign key references BlogPosts(BlogId)
)
GO
