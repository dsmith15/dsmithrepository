﻿USE KylesKookyCookieBlog
GO
IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetAllCategories')
BEGIN
   DROP PROCEDURE GetAllCategories
END
GO
CREATE PROCEDURE GetAllCategories
AS
    SELECT CategoryId, CategoryName
    FROM Categories c
GO
IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetCategoriesById')
BEGIN
   DROP PROCEDURE GetCategoriesById
END
GO
CREATE PROCEDURE GetCategoriesById
    @CategoryId int
AS
    SELECT CategoryId, CategoryName
    FROM Categories c
    WHERE CategoryId = @CategoryId
GO
IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetCategoriesByName')
BEGIN
   DROP PROCEDURE GetCategoriesByName
END
GO
CREATE PROCEDURE GetCategoriesByName
    @CategoryName VARCHAR(30)
AS
    SELECT CategoryId, CategoryName
    FROM Categories
    WHERE CategoryName LIKE @CategoryName
GO
IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'CreateCategories')
BEGIN
   DROP PROCEDURE CreateCategories
END
GO
CREATE PROCEDURE CreateCategories
    @CategoryName VARCHAR(30)
AS
    INSERT INTO Categories (CategoryName)
    VALUES (@CategoryName);
GO
IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'UpdateCategories')
BEGIN
   DROP PROCEDURE UpdateCategories
END
GO
CREATE PROCEDURE UpdateCategories
    @CategoryName VARCHAR(30),
    @CategoryId INT
AS
    UPDATE Categories
    SET CategoryName = @CategoryName
    WHERE CategoryId = @CategoryId
GO
IF EXISTS(
   SELECT *
   FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'DeleteCategories')
BEGIN
   DROP PROCEDURE DeleteCategories
END
GO
CREATE PROCEDURE DeleteCategories
    @CategoryId INT
AS
    DELETE FROM Categories
    WHERE CategoryId = @CategoryId
GO